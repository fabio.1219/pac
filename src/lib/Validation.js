import Validator from 'validatorjs'

export default function valider (data, { attributeNames, messagesErreur, rules }) {
  let validator = new Validator(data, rules, messagesErreur)
  validator.setAttributeNames(attributeNames)
  const reussite = validator.passes()
  const validation = flattenValidation(validator.errors.all())
  return { reussite, validation }
}

function flattenValidation (validation) {
  let newValidation = {}
  Object.keys(validation).forEach(function (key) {
    newValidation[key] = validation[key][0]
  })
  return newValidation
}
