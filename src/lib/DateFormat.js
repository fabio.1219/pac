const addZero = (number) => {
    if (number < 10) { number = '0' + number }
    return number
  }

export const getDate = () => {
    let currentDate = new Date()
    let day = currentDate.getDate()
    let month = currentDate.getMonth() + 1
    let year = currentDate.getFullYear()
    return `${addZero(day)}.${addZero(month)}.${year}`
  }

export const getHeure = () => {
    let date = new Date()
    let minutes = date.getUTCMinutes()
    let hour = date.getHours()
    return `${addZero(hour)}:${addZero(minutes)}`
  }

export const formateDateComptoDate = dateComp => {
  const dateAndTime = dateComp.split(' ')
  const date = dateAndTime[0]
  const [year, month, day] = date.split('-')
  return `${day}.${month}.${year}`
}

const getDateHeure = () => {
  return `${getDate()} ${getHeure()}`
}

export default getDateHeure