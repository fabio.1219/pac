const createCookie = (name, value, days) => {
  let date = new Date()
  date.setTime(date.getTime() + days * 24 * 60 * 60 * 1000)
  document.cookie = `${name}=${value};expires=${date};path=/`
}

export const saveInCookie = token => {
  createCookie('token', token, 365)
}

export const saveInSessionStorage = token => {
  window.sessionStorage.token = token
}

export const clear = () => {
  window.sessionStorage.clear()
  createCookie('', '', -1)
}

export const getSavedToken = () => {
  if (window.sessionStorage.token) {
    return window.sessionStorage.token
  }

  if (document.cookie && document.cookie.length > 0) {
    let key = 'token='
    let cookies = decodeURIComponent(document.cookie).split(';')
    for (let cookie of cookies) {
      if (cookie.indexOf(key) === 0) {
        return cookie.substring(key.length, cookie.length)
      }
    }
  }

  return null
}
