import React from 'react'
import Categories from 'components/global/Categories'
import Select from 'components/global/Select'

const Filtres = ({
  labels,
  categorie,
  handleOnClickCategorie,
  handleOnChangeReferant,
  referants,
  referant
}) => {
  return (
    <div>
      <h3>Catégories de client</h3>
      <Categories
        labels={labels}
        modeSelection
        selected={[categorie]}
        handleOnClick={handleOnClickCategorie}
      />
      <div className='form-field'>
        <label>Référant</label>
        <Select
          name='referant'
          value={referant.id}
          options={referants}
          onChange={handleOnChangeReferant}
        >
          <option value={-1}>Afficher un référant...</option>
          {referants.map((referant, index) => (
            <option value={referant.id} key={referant.id}>{referant.nom}</option>
          ))}
        </Select>
      </div>
    </div>
  )
}

export default Filtres
