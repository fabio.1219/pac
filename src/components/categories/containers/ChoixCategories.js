import React, { Component } from 'react'
import Filtres from '../presentationals/Filtres'
import { connect } from 'react-redux'
import { actions } from 'redux/categories'
import { actions as actionsReferants } from 'redux/referants'

const LABELS = ['Sans catégorie', '1', '2', '3', '4', '5', '6', '7', '8', '9']

class ChoixCategories extends Component {
  componentDidMount () {
    this.props.getReferants()
  }

  actualiserAffichage = (categorie, referant) => {
    const { getClientsCategorie } = this.props
    getClientsCategorie(categorie, referant)
  }

  handleOnClickCategorie = e => {
    const categorie = +e.target.value
    this.props.choixCategorie({ categorie })
    this.actualiserAffichage(categorie, this.props.referant.id)
  }

  handleOnChangeReferant = e => {
    const referant = e.target.value
    this.props.choixReferant({ referant })
    this.actualiserAffichage(this.props.categorie, referant.id)
  }

  render () {
    const { categorie, referant, referants } = this.props
    return (
      <Filtres
        labels={LABELS}
        categorie={categorie}
        handleOnClickCategorie={this.handleOnClickCategorie}
        handleOnChangeReferant={this.handleOnChangeReferant}
        referants={referants}
        referant={referant}
      />
    )
  }
}

const mapStateToProps = state => {
  return {
    categorie: state.categories.categorie,
    referant: state.categories.referant,
    referants: state.referants.referants
  }
}

const mapDispatchToProps = {
  choixCategorie: actions.choixCategorie,
  choixReferant: actions.choixReferant,
  getClientsCategorie: actions.getClientsCategorie,
  getReferants: actionsReferants.getReferants
}

ChoixCategories = connect(mapStateToProps, mapDispatchToProps)(ChoixCategories)

export default ChoixCategories
