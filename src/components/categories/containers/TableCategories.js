import React, { Component } from 'react'
import { connect } from 'react-redux'
import Table from '../../global/Table'
import Pagination from '../../global/pagination'
import { actions } from '../../../redux/categories'
import Loader from 'components/global/loader/Loader'

const ENTETES = ['ID', 'Nom', 'Groupe']
const KEYS = [client => client.id, client => client.nom, client => client.groupe]

class TableCategories extends Component {
  handleOnClick = e => {
    e.persist()
    e.preventDefault()
    let { categorie, referant } = this.props.categories
    this.props.getClientsCategorie(categorie, referant.id, {
      page: e.target.name[e.target.name.length - 1]
    })
  }

  handleOpen = (e, client, id) => {
    this.props.history.push(`/clients/${client.id}`)
  }

  affichageTitre = (categorie, referant) => {
    let base = 'Clients'

    if (categorie === 0) {
      base += ' sans catégorie'
    } else if (categorie > 0) {
      base += ` pour la catégorie n°${categorie}`
    }

    if (referant.id > -1) {
      base += ' de ' + referant.nom
    }

    return base
  }

  afficherTableau = () => {
    let {
      isFetching,
      from,
      to,
      total,
      current_page,
      last_page,
      prev_page_url,
      next_page_url,
      data
    } = this.props.categories
    if (!isFetching && data.length > 0) {
      return (
        <div>
          <div className='table-info'>
            <div className='table-info-no'>{from} - {to}</div>
            <div className='table-info-text'> clients sur </div>
            <div className='table-info-no'>{total}</div>
          </div>
          <Table entetes={ENTETES} rows={data} keys={KEYS} handleOpen={this.handleOpen} />
          <Pagination
            prev_page_url={prev_page_url}
            next_page_url={next_page_url}
            current_page={current_page}
            last_page={last_page}
            handleOnClick={this.handleOnClick}
          />
        </div>
      )
    }
    if (isFetching) {
      return <Loader />
    }
    if (data.length === 0) {
      return <div className='card-content notfound-p'><p>Aucun client trouvé.</p></div>
    }
  }

  render () {
    let { categorie, referant } = this.props.categories
    return (
      <div>
        <h3>{this.affichageTitre(categorie, referant)}</h3>
        {this.afficherTableau()}
      </div>
    )
  }
}

const mapState = state => {
  return {
    categories: state.categories
  }
}

const mapDispatch = {
  getClientsCategorie: actions.getClientsCategorie,
  reinit: actions.reinit
}

TableCategories = connect(mapState, mapDispatch)(TableCategories)

export default TableCategories
