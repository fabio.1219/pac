import React from 'react'
import Logo from '../../assets/logo.png'
import { NavLink } from 'react-router-dom'
import { connect } from 'react-redux'
import { withRouter } from 'react-router'

const Menu = ({ items, onClickLogout, typeUtilisateur }) => {
  return (
    <nav className='sidemenu'>
      <NavLink to={'/'} className='sidemenu-image'>
        <img className='image' alt='pac' src={Logo} />
      </NavLink>
      {items.map(
        item =>
          (typeUtilisateur <= item.authorization
            ? <NavLink
              to={`/${item.path}`}
              className='sidemenu-item'
              key={item.name}
              activeClassName='sidemenu-item--active'
              >
              {item.name}<div className='fa fa-angle-right icon' />
            </NavLink>
            : null)
      )}
      <NavLink
        to={'/login'}
        className='sidemenu-item sidemenu-item--logout'
        onClick={onClickLogout}
      >
        Déconnexion
      </NavLink>
    </nav>
  )
}

const mapStateToProps = state => {
  return {
    typeUtilisateur: state.login.decoded.type
  }
}

const mapDispatchToProps = null

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(Menu))
