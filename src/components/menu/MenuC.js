import React, { Component } from 'react'
import Menu from './Menu'
import { RESPONSABLE_COMMERICAL, RESPONSABLE_SAISIE, NORMAL } from 'conf/typeUtilisateur'
import { connect } from 'react-redux'
import { actions } from 'redux/login'

import { withRouter } from 'react-router'

const ITEMS = [
  {
    name: 'Utilisateurs',
    path: 'utilisateurs',
    authorization: RESPONSABLE_SAISIE
  },
  {
    name: 'Clients',
    path: 'clients',
    authorization: RESPONSABLE_SAISIE
  },
  {
    name: 'Catégories',
    path: 'categories',
    authorization: NORMAL
  },
  {
    name: "Types d'actions",
    path: 'typesactions',
    authorization: RESPONSABLE_SAISIE
  },
  {
    name: 'Actions',
    path: 'actions',
    authorization: NORMAL
  },
  {
    name: 'Statistiques',
    path: 'statistiques',
    authorization: RESPONSABLE_COMMERICAL
  }
]

class MenuC extends Component {
  onClickLogout = e => {
    this.props.logout()
  }

  render () {
    return <Menu items={ITEMS} onClickLogout={this.onClickLogout} />
  }
}

const mapStateToProps = null

const mapDispatchToProps = { logout: actions.logout }

MenuC = connect(mapStateToProps, mapDispatchToProps)(MenuC)

export default withRouter(MenuC)
