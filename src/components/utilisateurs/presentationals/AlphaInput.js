import React, { Component } from 'react'

class AlphaInput extends Component {
  handleOnChange = e => {
    const { name, value } = e.target
    const newValue = value.trim().toLowerCase().replace(/([^a-zA-Z ]|\s)/g, '')
    const newEvent = { target: { name, value: newValue } }
    this.props.onChange(newEvent)
  }

  render () {
    const { name, type, value, placeholder, readOnly } = this.props
    return (
      <input
        name={name}
        type={type}
        value={value}
        placeholder={placeholder}
        onChange={this.handleOnChange}
        readOnly={readOnly}
      />
    )
  }
}

export default AlphaInput
