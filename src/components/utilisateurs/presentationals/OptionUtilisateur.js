import React from 'react'
import { Link } from 'react-router-dom'

const OptionUtilisateur = ({ nom, handleOnChange }) => {
  return (
    <div className='card-content container-2-elements'>
      <div className='container-2-elements--first align-to-button--input'>
        <div className='icon-input'>
          <input
            type='text'
            name='nom'
            placeholder='Chercher par nom'
            value={nom}
            onChange={handleOnChange}
          />
          <i className='fa fa-search' aria-hidden='true' />
        </div>
      </div>
      <Link
        to={'/utilisateurs/ajouter'}
        className='container-2-elements--second'
        style={{ textDecoration: 'none' }}
      >
        <button className='button'>Ajouter un utilisateur</button>
      </Link>
    </div>
  )
}

export default OptionUtilisateur
