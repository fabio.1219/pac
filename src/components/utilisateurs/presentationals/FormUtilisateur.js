import React from 'react'
import Field from 'components/global/Field'
import Text from 'components/global/Text'
import AlphaInput from './AlphaInput'
import { RESPONSABLE_COMMERICAL, RESPONSABLE_SAISIE, REFERANT, NORMAL, NOM_TYPE } from 'conf/typeUtilisateur'

const FormUtilisateur = ({
  nom,
  login,
  type,
  typeUtilisateur,
  handleOnChange,
  validation,
  validationId,
  isDisabled
}) => {
  return (
    <div className='card-content'>
      <h3>Informations de l'utilisateur</h3>
      <div className='form row'>
        <div className='form-column _50'>
          <Field label={'Nom'} validation={validation['nom']}>
            {isDisabled
              ? <Text>{nom}</Text>
              : <input
              type='text'
              name='nom'
              placeholder='30 caractères maximum.'
              maxLength={30}
              value={nom}
              onChange={handleOnChange}
            />}
          </Field>
          <Field label={'Identifiant'} validation={validation['login'] || validationId}>
            {isDisabled
              ? <Text>{login}</Text>
              : <AlphaInput
              name='login'
              type='text'
              placeholder='Seuls les caractères alphabétiques sont acceptés.'
              value={login}
              onChange={handleOnChange}
            />}
          </Field>
          <Field label={'Type'} validation={validation['type']}>
            {isDisabled
              ? <Text>{NOM_TYPE[type]}</Text>
              : <div className='form-select'>
              <select value={type} name='type' onChange={handleOnChange}>
                <option value=''>Choisir un type d'utilisateur...</option>
                {typeUtilisateur === 0 && <option value={RESPONSABLE_COMMERICAL}>Responsable commercial</option>}
                <option value={RESPONSABLE_SAISIE}>Responsable saisie</option>
                <option value={REFERANT}>Référant</option>
                <option value={NORMAL}>Utilisateur normal</option>
              </select>
            </div>}
          </Field>
        </div>
      </div>
    </div>
  )
}

export default FormUtilisateur
