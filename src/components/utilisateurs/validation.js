const validation = {
  attributeNames: {
    nom: "Le nom d'utilisateur",
    login: "L'identifiant de l'utilisateur",
    type: "Le type d'utilisateur"
  },
  messagesErreur: {
    required: ':attribute est obligatoire.'
  },
  rules: {
    nom: 'required',
    login: 'required',
    type: 'required'
  }
}

export default validation
