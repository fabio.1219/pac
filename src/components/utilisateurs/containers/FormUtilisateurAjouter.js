import React, { Component } from 'react'
import FormUtilisateur from '../presentationals/FormUtilisateur'
import FormHandlerHOC from 'components/global/FormHandlerHOC'
import Buttons from 'components/global/Buttons'
import Loader from 'components/global/loader/Loader'
import valider from 'lib/Validation'
import validation from '../validation'

import { connect } from 'react-redux'
import { actions } from 'redux/utilisateurs'

class FormUtilisateurAjouter extends Component {
  constructor (props) {
    super(props)
    this.FormHandled = FormHandlerHOC(this.updateFormUtilisateurNew)(FormUtilisateur)
    this.state = {
      buttons: [
        {
          label: 'Enregistrer',
          handleOnClick: this.handleOnClickEnregistrer
        }
      ],
      validation: {}
    }
  }
  
  updateFormUtilisateurNew = ({ change }) => {
    if (change.nom) {
      change.login = change.nom.trim().toLowerCase().replace(/([^a-zA-Z ]|\s)/g, '')
    }
    this.props.updateFormUtilisateurNew({ change })
  }

  handleOnClickEnregistrer = async e => {
    e.preventDefault()
    const resultat = valider(this.props.utilisateurNew, validation)
    if (resultat.reussite) {
      const {payload} = await this.props.addUtilisateur(this.props.utilisateurNew)
      if (payload.response.status === 'success') {
        this.setState({ validation: {} })
        this.props.history.push(`/utilisateurs/${payload.response.id}`)
      }
    } else {
      this.setState({ validation: resultat.validation })
    }
  }

  render () {
    const FormHandled = this.FormHandled
    const { utilisateurNew, validationId, type, isFetching } = this.props
    const { buttons, validation } = this.state
    if (isFetching) {
      return <Loader />
    }

    if (utilisateurNew) {
      return (
        <div>
          <FormHandled form={utilisateurNew} validation={validation} validationId={validationId} typeUtilisateur={type} isDisabled={false} />
          <div className='all-separateur' />
          <section className='card-content'>
            <Buttons buttons={buttons} />
          </section>
        </div>
      )
    } else {
      return <div />
    }
  }
}

const mapState = state => {
  return {
    isFetching: state.utilisateurs.isFetching,
    utilisateurNew: state.utilisateurs.utilisateurNew,
    validationId: state.utilisateurs.utilisateurNew.validationId,
    type: state.login.decoded.type
  }
}

const mapDispatch = {
  updateFormUtilisateurNew: actions.updateFormUtilisateurNew,
  addUtilisateur: actions.addUtilisateur
}

FormUtilisateurAjouter = connect(mapState, mapDispatch)(FormUtilisateurAjouter)

export default FormUtilisateurAjouter
