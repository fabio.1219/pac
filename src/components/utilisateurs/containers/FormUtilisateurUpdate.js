import React, { Component } from 'react'
import FormUtilisateur from '../presentationals/FormUtilisateur'
import FormHandlerHOC from 'components/global/FormHandlerHOC'
import ModalSupprimer from 'components/global/ModalSupprimer'
import valider from 'lib/Validation'
import validation from '../validation'
import Loader from 'components/global/loader/Loader'

import { connect } from 'react-redux'
import { actions } from 'redux/utilisateurs'
import { Redirect } from 'react-router-dom'

class FormUtilisateurUpdate extends Component {
  updateFormUtilisateurEdited = ({ change }) => {
    if (change.nom) {
      change.login = change.nom.trim().toLowerCase().replace(/([^a-zA-Z ]|\s)/g, '')
    }
    this.props.updateFormUtilisateurEdited({ change })
  }

  FormHandled = FormHandlerHOC(this.updateFormUtilisateurEdited)(FormUtilisateur)

  state = {
    isDisabled: true,
    validation: {},
    showModal: false,
    redirect: false
  }

  renderButtonModifier = () => (
    <div>
    <button className='button' onClick={this.handleOnClickModifier}>Modifier</button>
    <button
        className='simple-button-icon simple-button-icon--red'
        onClick={this.handleShowModal}
      >
        Supprimer
      </button>
    </div>
  )

  renderButtonsEnregistrer = () => (
    <div>
      <button className='button' onClick={this.handleOnClickEnregistrer}>Enregistrer</button>
      <button
        className='simple-button-icon simple-button-icon--red'
        onClick={this.handleOnClickAnnuler}
      >
        Annuler
      </button>
    </div>
  )

  handleOnClickModifier = e => {
    e.preventDefault()
    this.setState({
      isDisabled: false
    })
  }

  handleOnClickAnnuler = e => {
    e.preventDefault()
    this.setState({
      isDisabled: true,
      validation: {}
    })
  }

  handleShowModal = e => { this.setState({showModal: true })}
  handleHideModal = e => { this.setState({showModal: false })}

  handleOnClickSupprimer = async e => {
    e.preventDefault()
    const { utilisateurEdited, delUtilisateur } = this.props
    await delUtilisateur(utilisateurEdited.id)
    this.setState({
      redirect: true
    })
  }

  handleOnClickEnregistrer = e => {
    e.preventDefault()
    const resultat = valider(this.props.utilisateurEdited, validation)
    if (resultat.reussite) {
      this.props.updateUtilisateur(this.props.utilisateurEdited.id, this.props.utilisateurEdited)
      this.setState({
        isDisabled: true,
        validation: {}
      })
    } else {
      this.setState({ validation: resultat.validation })
    }
  }

  render () {
    const FormHandled = this.FormHandled
    const { utilisateurEdited, validationId, isFetching, type } = this.props
    const { isDisabled, validation, redirect, showModal } = this.state
    if (isFetching) {
      return <Loader />
    }

    if (redirect) {
      return <Redirect to='/utilisateurs' />
    }

    if (utilisateurEdited) {
      return (
        <div>
          <FormHandled
            form={utilisateurEdited}
            validation={validation}
            validationId={validationId}
            typeUtilisateur={type}
            isDisabled={isDisabled}
          />
          <div className='all-separateur' />
          <section className='card-content'>
            {isDisabled ? this.renderButtonModifier() : this.renderButtonsEnregistrer()}
          </section>
          {showModal && <ModalSupprimer handleOnClickAnnuler={this.handleHideModal} handleOnClickSupprimer={this.handleOnClickSupprimer} />}
        </div>
      )
    } else {
      return <div />
    }
  }
}

const mapState = state => {
  return {
    utilisateurEdited: state.utilisateurs.utilisateurEdited,
    validationId: state.utilisateurs.utilisateurEdited.validationId,
    isFetching: state.utilisateurs.isFetching,
    type: state.login.decoded.type
  }
}

const mapDispatch = {
  updateFormUtilisateurEdited: actions.updateFormUtilisateurEdited,
  updateUtilisateur: actions.updateUtilisateur,
  delUtilisateur: actions.delUtilisateur
}

FormUtilisateurUpdate = connect(mapState, mapDispatch)(FormUtilisateurUpdate)

export default FormUtilisateurUpdate
