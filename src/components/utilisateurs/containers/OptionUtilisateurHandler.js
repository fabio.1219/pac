import React, { Component } from 'react'
import OptionUtilisateur from '../presentationals/OptionUtilisateur'
import { connect } from 'react-redux'
import { actions } from 'redux/utilisateurs'

let timeoutID

class OptionUtilisateurHandler extends Component {
  constructor (props) {
    super(props)
    this.state = {
      search: ''
    }
  }

  handleOnChange = e => {
    clearTimeout(timeoutID)
    e.preventDefault()
    let getUtilisateurs = search =>
      (timeoutID = setTimeout(() => {
        this.props.getUtilisateurs({ nom: search, page: 1 })
      }, 750))
    this.setState({ search: e.target.value }, () => getUtilisateurs(this.state.search))
  }

  render () {
    let { search } = this.props
    return <OptionUtilisateur search={search} handleOnChange={this.handleOnChange} />
  }
}

const mapDispatch = { getUtilisateurs: actions.getUtilisateurs }

OptionUtilisateurHandler = connect(null, mapDispatch)(OptionUtilisateurHandler)

export default OptionUtilisateurHandler
