import React, { Component } from 'react'
import { connect } from 'react-redux'
import Table from 'components/global/Table'
import Pagination from 'components/global/pagination'
import Loader from 'components/global/loader/Loader'
import { actions } from 'redux/utilisateurs'
import { NOM_TYPE } from 'conf/typeUtilisateur'

const ENTETES = ['N°', 'Nom', 'Identifiant', 'Type']
const KEYS = [
  utilisateur => utilisateur.id,
  utilisateur => utilisateur.nom,
  utilisateur => utilisateur.login,
  utilisateur => NOM_TYPE[utilisateur.type]
]

class TableUtilisateurs extends Component {
  handleOnClick = e => {
    e.preventDefault()
    this.props.getUtilisateurs({ page: e.target.name[e.target.name.length - 1] })
  }

  handleOpen = (e, object, index) => {
    e.preventDefault()
    this.props.history.push(`/utilisateurs/${object.id}`)
  }

  componentWillMount () {
    this.props.getUtilisateurs()
  }

  render () {
    let {
      isFetching,
      from,
      to,
      total,
      current_page,
      last_page,
      prev_page_url,
      next_page_url,
      data
    } = this.props.utilisateurs
    if (!isFetching && data.length > 0) {
      return (
        <div>
          <div className='table-info'>
            <div className='table-info-no'>{from} - {to}</div>
            <div className='table-info-text'> clients sur </div>
            <div className='table-info-no'>{total}</div>
          </div>
          <Table entetes={ENTETES} rows={data} keys={KEYS} handleOpen={this.handleOpen} />
          <Pagination
            prev_page_url={prev_page_url}
            next_page_url={next_page_url}
            current_page={current_page}
            last_page={last_page}
            handleOnClick={this.handleOnClick}
          />
        </div>
      )
    } else if (isFetching) {
      return <Loader />
    } else {
      return <div className='notfound-p'><p>Aucun utilisateur trouvée.</p></div>
    }
  }
}

const mapState = state => {
  return {
    utilisateurs: state.utilisateurs
  }
}

const mapDispatch = { getUtilisateurs: actions.getUtilisateurs }

TableUtilisateurs = connect(mapState, mapDispatch)(TableUtilisateurs)

export default TableUtilisateurs
