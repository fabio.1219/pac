import React, { Component } from 'react'
import OptionClient from '../presentationals/OptionClient'
import { connect } from 'react-redux'
import { actions } from '../../../redux/clients'

let timeoutID

class OptionClientHandler extends Component {
  constructor (props) {
    super(props)
    this.state = {
      search: ''
    }
  }

  handleOnChange = e => {
    clearTimeout(timeoutID)
    e.preventDefault()
    let getClients = search =>
      (timeoutID = setTimeout(() => {
        this.props.getClients({ nom: search, page: 1 })
      }, 600))
    this.setState({ search: e.target.value }, () => getClients(this.state.search))
  }

  render () {
    let { search } = this.props
    return <OptionClient search={search} handleOnChange={this.handleOnChange} />
  }
}

const mapDispatch = { getClients: actions.getClients }

OptionClientHandler = connect(null, mapDispatch)(OptionClientHandler)

export default OptionClientHandler
