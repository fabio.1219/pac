import React, { Component } from 'react'
import { connect } from 'react-redux'
import Table from '../../global/Table'
import Pagination from '../../global/pagination'
import { actions } from '../../../redux/clients'
import Loader from 'components/global/loader/Loader'

const ENTETES = ['ID', 'Nom', 'Groupe', 'Catégorie']
const KEYS = [
  client => client.id,
  client => client.nom,
  client => client.groupe,
  client => (
    <div className={`badge-categorie badge-categorie--${client.categorie}`}>{client.categorie}</div>
  )
]

class TableClients extends Component {
  handleOnClick = e => {
    e.preventDefault()
    this.props.getClients({ page: e.target.name[e.target.name.length - 1] })
  }

  componentWillMount () {
    this.props.getClients()
  }

  handleOpen = (e, object, id) => {
    this.props.history.push(`/clients/${object.id}`)
  }

  render () {
    let {
      isFetching,
      from,
      to,
      total,
      current_page,
      last_page,
      prev_page_url,
      next_page_url,
      data
    } = this.props.clients
    if (!isFetching && data.length > 0) {
      return (
        <div className='card-content'>
          <div className='table-info'>
            <div className='table-info-no'>{from} - {to}</div>
            <div className='table-info-text'> clients sur </div>
            <div className='table-info-no'>{total}</div>
          </div>
          <Table entetes={ENTETES} rows={data} keys={KEYS} handleOpen={this.handleOpen} />
          <Pagination
            prev_page_url={prev_page_url}
            next_page_url={next_page_url}
            current_page={current_page}
            last_page={last_page}
            handleOnClick={this.handleOnClick}
          />
        </div>
      )
    } else if (isFetching) {
      return <Loader />
    } else {
      return <div className='card-content notfound-p'><p>Aucun client trouvé.</p></div>
    }
  }
}

const mapState = state => {
  return {
    clients: state.clients
  }
}

const mapDispatch = { getClients: actions.getClients }

TableClients = connect(mapState, mapDispatch)(TableClients)

export default TableClients
