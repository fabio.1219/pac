import React from 'react'
import { Link } from 'react-router-dom'

const OptionClient = ({ nom, handleOnChange }) => {
  return (
    <div className='card-content container-2-elements'>
      <div className='icon-input container-2-elements--first  align-to-button--input'>
        <input
          type='text'
          placeholder='Chercher un client'
          name='nom'
          value={nom}
          onChange={handleOnChange}
        />
        <i className='fa fa-search' aria-hidden='true' />
      </div>
      <Link
        to={'/clients/ajouter'}
        className='container-2-elements--second'
        style={{ textDecoration: 'none' }}
      >
        <button className='button'>Ajouter un client</button>
      </Link>
    </div>
  )
}

export default OptionClient
