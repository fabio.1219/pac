const validationClient = {
      formClient: {
        attributeNames: { 
        nom: 'Le nom du client',
        groupe: 'Le groupe de client',
        secteur: 'Le secteur',
        referant: 'Un référant'
        },
        messagesErreur: {
            required: ':attribute est obligatoire.',
            'min.categorie': 'Il faut attribuer une catégorie au client.'
        },
        rules: {
          nom: 'required',
          groupe: 'required',
          secteur: 'required',
          referant: 'required',
          categorie: 'min:0'
        }
      },
      formContact: {
        attributeNames: { 
          nom: 'Le nom du contact',
          telephone: 'Le numéro de téléphone',
          email: 'L\'adresse e-email'
        },
        messagesErreur: {
            required: ':attribute est obligatoire.',
            'min.telephone': ':attribute n\'est pas valide.',
            email: ':attribute n\'est pas valide.'
        },
        rules: {
          nom: 'required',
          telephone: 'min:6',
          email: 'email'
        }
      }
    }

    export default validationClient