import TabsHOC from 'components/global/TabsHOC'
import PartieClient from './PartieClient'
import PartieContacts from './PartieContacts'
import ComptesrendusC from './ComptesrendusC'
import React, { Component } from 'react'
import { actions } from 'redux/client'
import { connect } from 'react-redux'
import Loader from 'components/global/loader/Loader'

const TABSBASE = [
  {
    nom: 'PartieClient',
    nomFormat: 'Informations du client',
    component: PartieClient
  },
  {
    nom: 'PartieContacts',
    nomFormat: 'Contacts',
    component: PartieContacts
  }
]

const TABS = [
  ...TABSBASE,
  {
    nom: 'ComptesrendusC',
    nomFormat: 'Comptes-rendus',
    component: ComptesrendusC
  }
]

class TabsClient extends Component {
  componentWillMount () {
    this.props.getReferants()
  }

  render () {
    const { setEtatTab, etatTab, editMode, isFetching } = this.props
    if (isFetching) {
      return <Loader />
    }

    const Tabs = TabsHOC(editMode ? TABSBASE : TABS, setEtatTab)
    return <Tabs etat={etatTab} history={this.props.history} />
  }
}

export default connect(
  state => {
    return {
      editMode: state.client.editMode,
      etatTab: state.client.etatTab,
      isFetching: state.client.isFetching
    }
  },
  { setEtatTab: actions.setEtatTab, getReferants: actions.getReferants }
)(TabsClient)
