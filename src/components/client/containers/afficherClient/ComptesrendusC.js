import React, { Component } from 'react'
import Comptesrendus from '../../presentationals/Comptesrendus'
import { connect } from 'react-redux'
import { actions } from 'redux/client'

class ComptesrendusC extends Component {
  state = {
    compterendu: ''
  }

  handleOnChange = e => {
    const target = e.target
    const value = target.value
    const name = target.name
    const change = { [name]: value }
    this.setState(change)
  }

  handleOnClick = async e => {
    const { compterendu } = this.state
    const { id, postCompterendu, getClient } = this.props
    this.setState({ compterendu: '' })
    await postCompterendu({ client: id, texte: compterendu })
    await getClient(id)
  }

  render () {
    const { comptesrendus } = this.props
    return (
      <Comptesrendus
        comptesrendus={comptesrendus}
        compterendu={this.state.compterendu}
        handleOnChange={this.handleOnChange}
        handleOnClick={this.handleOnClick}
      />
    )
  }
}

const mapStateToProps = state => {
  return {
    id: state.client.clientEdited.id,
    comptesrendus: state.client.clientEdited.comptesrendus
  }
}

const mapDispatchToProps = {
  postCompterendu: actions.postCompterendu,
  getClient: actions.getClient
}

ComptesrendusC = connect(mapStateToProps, mapDispatchToProps)(ComptesrendusC)

export default ComptesrendusC
