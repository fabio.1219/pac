import React, { Component } from 'react'
import FormContacts from '../../presentationals/FormContacts'
import ButtonsClient from './ButtonsClient'
import valider from 'lib/Validation'
import validationClient from '../../validation'
import { connect } from 'react-redux'
import { actions } from 'redux/client'

class PartieContacts extends Component {
  constructor (props) {
    super(props)
    this.state = {
      form: {
        nom: '',
        telephone: '',
        email: ''
      },
      selected: -1,
      validation: {}
    }
  }

  handleOnChange = e => {
    this.setState({
      form: {
        ...this.state.form,
        [e.target.name]: e.target.value
      }
    })
  }

  handleOpen = (e, object, index) => {
    e.preventDefault()
    this.setState({
      form: object,
      selected: index,
      validation: {}
    })
  }

  resetForm = () => {
    this.setState({
      form: {
        nom: '',
        telephone: '',
        email: ''
      },
      selected: -1,
      validation: {}
    })
  }

  handleOnClickCancel = e => {
    e.preventDefault()
    this.resetForm()
  }

  handleDelete = (e, object, index) => {
    e.preventDefault()
    this.props.deleteContact({index})
    this.resetForm()
  }

  handleOnClickModif = e => {
    const resultat = valider(this.state.form, validationClient.formContact)
    if (resultat.reussite) {
      this.props.updateContact({contact: this.state.form, index: this.state.selected})
      this.resetForm()
    } else {
      this.setState({ validation: resultat.validation })
    }
  }

  handleOnClickAdd = e => {
    const resultat = valider(this.state.form, validationClient.formContact)
    if (resultat.reussite) {
      this.props.addContact({contact: this.state.form})
      this.resetForm()
    } else {
      this.setState({ validation: resultat.validation })
    }
  }

  render () {
    const { form, selected, validation } = this.state
    const { contacts, editMode } = this.props
    const contactsFiltered = contacts.filter(contact => contact.deleted !== true)
    return (
      <div>
      <FormContacts
        contacts={contactsFiltered}
        editMode={editMode}
        validation={validation}
        handleOnChange={this.handleOnChange}
        handleOpen={this.handleOpen}
        handleDelete={this.handleDelete}
        selected={selected !== -1}
        form={form}
        handleOnClickCancel={this.handleOnClickCancel}
        handleOnClickModif={this.handleOnClickModif}
        handleOnClickAdd={this.handleOnClickAdd}
      />
      <ButtonsClient history={this.props.history} />
      </div>
    )
  }
}

PartieContacts = connect(
  state => {
    return {
      contacts: state.client.clientEdited.contacts,
      editMode: state.client.editMode
    }
  },
  {
    deleteContact: actions.deleteContact,
    updateContact: actions.updateContact,
    addContact: actions.addContact
  }
)(PartieContacts)

export default PartieContacts
