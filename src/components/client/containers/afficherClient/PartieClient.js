import React, { Component } from 'react'
import Form from '../../presentationals/Form'
import FormHandlerHOC from 'components/global/FormHandlerHOC'
import ButtonsClient from './ButtonsClient'

import { connect } from 'react-redux'
import { actions } from 'redux/client'

class PartieClient extends Component {
  constructor (props) {
    super(props)
    this.FormHandled = FormHandlerHOC(this.props.updateFormClient)(Form)
  }

  handleOnClickCategorie = e => {
    e.preventDefault()
    if (this.props.editMode) {
      this.props.updateFormClient({ change: { categorie: +e.target.value } })
    }
  }

  render () {
    const { clientEdited, editMode, validation, referants, isFetching } = this.props
    const FormHandled = this.FormHandled
    if (!isFetching) {
      return (
        <div>
          <FormHandled
            form={clientEdited}
            referants={referants}
            validation={validation}
            modeSelection
            isDisabled={!editMode}
            selected={[clientEdited.categorie]}
            handleOnClickCategorie={this.handleOnClickCategorie}
          />
          <ButtonsClient history={this.props.history} />
        </div>
      )
    } else {
      return <div />
    }
  }
}

const mapState = state => {
  return {
    validation: state.client.validation,
    clientEdited: state.client.clientEdited,
    editMode: state.client.editMode,
    referants: state.client.referants,
    isFetching: state.client.isFetching
  }
}

const mapDispatch = {
  updateFormClient: actions.updateFormClient
}

PartieClient = connect(mapState, mapDispatch)(PartieClient)

export default PartieClient
