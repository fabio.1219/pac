import React, { Component } from 'react'
import ModalSupprimer from 'components/global/ModalSupprimer'
import valider from 'lib/Validation'
import validationClient from '../../validation'
import { connect } from 'react-redux'
import { actions } from 'redux/client'
import { Redirect } from 'react-router-dom'

class ButtonsClient extends Component {
  state = {
    redirectEntreprendre: false,
    showModal: false
  }

  renderButtons = () => {
    const { cancelEditClient, editMode, editModeClient, clientEdited } = this.props
    if (editMode) {
      return (
        <div>
          <button className='button' onClick={this.handleOnClickEnregistrer}>Enregistrer</button>
          <button className='simple-button-icon simple-button-icon--red' onClick={cancelEditClient}>Annuler</button>
        </div>
      )
    } else {
      return (
        <div>
          {clientEdited.categorie > 0 && <button className='button' onClick={this.handleOnClickEntreprendre}>
            Entreprendre une action
          </button>}
          <button
            className='simple-button-icon simple-button-icon--primary'
            onClick={editModeClient}
          >
            Modifier
          </button>
          <button
            className='simple-button-icon simple-button-icon--red'
            onClick={this.handleShowModal}
          >
            Supprimer
          </button>
        </div>
      )
    }
  }

  handleShowModal = e => { this.setState({showModal: true })}
  handleHideModal = e => { this.setState({showModal: false })}

  handleOnClickEntreprendre = e => {
    this.setState({ redirectEntreprendre: true })
  }

  handleOnClickSupprimer = async e => {
    e.preventDefault()
    const { clientEdited, delClient } = this.props
    await delClient(clientEdited.id)
    this.props.history.push('/clients')
  }

  handleOnClickEnregistrer = e => {
    const resultat = valider(this.props.clientEdited, validationClient.formClient)
    if (resultat.reussite) {
      this.props.updateClient(this.props.clientEdited.id, this.props.clientEdited)
    } else {
      this.props.validateClient({ validation: resultat.validation })
    }
  }

  render () {
    const id = this.props.clientEdited.id
    if (this.state.redirectEntreprendre) {
      return <Redirect to={`/actions/entreprendre/${id}`} />
    }
    return (
      <div>
        <div className='all-separateur' />
        <section className='card-content'>
          {this.renderButtons()}
        </section>
        {this.state.showModal && <ModalSupprimer handleOnClickAnnuler={this.handleHideModal} handleOnClickSupprimer={this.handleOnClickSupprimer} />}
      </div>
    )
  }
}

const mapState = state => {
  return {
    clientEdited: state.client.clientEdited,
    editMode: state.client.editMode
  }
}

const mapDispatch = {
  editModeClient: actions.editModeClient,
  validateClient: actions.validateClient,
  updateClient: actions.updateClient,
  cancelEditClient: actions.cancelEditClient,
  delClient: actions.delClient
}

ButtonsClient = connect(mapState, mapDispatch)(ButtonsClient)

export default ButtonsClient
