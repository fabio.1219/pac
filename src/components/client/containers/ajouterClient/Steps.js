import React, { Component } from 'react';
import Form from '../../presentationals/Form'
import FormContacts from '../../presentationals/FormContacts'
import PreverNexter from '../../presentationals/PreverNexter'
import valider from 'lib/Validation'
import validationClient from '../../validation'
import { connect } from 'react-redux'
import { actions } from 'redux/client'
import { postClient } from 'api/apiClient'
import Loader from 'components/global/loader/Loader'

class Steps extends Component {
  constructor(props) {
    super(props)
    this.state = {
      formClient: {        
        form: {
          nom: '',
          groupe: '',
          secteur: '',
          referant: '',
          categorie: -1
        },
        validation: {}
      },
      formContact: {
        indexSelected: -1,
        contacts: [],
        form: {
          nom: '',
          telephone: '',
          email: ''
        },
        validation: {},
        selected: false
      },
      step: 0
    }
  }

  componentWillMount () {
    this.props.getReferants()
  }
  
  /* Partie gérer form client */
  handleOnChangeClient = (e) => {
    const { name, value } = e.target
    this.setState({
      formClient: {
        ...this.state.formClient,
        form: {
          ...this.state.formClient.form,
          [name]: value
        }
    }})
  }

  handleOnClickCategorie = (e) => { //gerer le cas special des categories
    e.preventDefault()
      this.setState({
      formClient: {
        ...this.state.formClient,
      form: {
      ...this.state.formClient.form,
      categorie: +e.target.value 
      }
    }})
  }

  /* partie gérer form contact */
  handleOnChangeContact = (e) => {
    const { name, value } = e.target
    this.setState({
      formContact: {
        ...this.state.formContact,
        form: {
          ...this.state.formContact.form,
          [name]: value
        }
    }})
  }

  handleDelete = (e, object, index) => {
    e.preventDefault()
    let contacts = this.state.formContact.contacts.slice()
    contacts.splice(index, 1)
    if (this.state.formContact.selected) {
      this.setState({
        formContact: {
          contacts,
          indexSelected: -1,
          form: {nom: '', telephone: '', email: ''},
          validation: {},
          selected: false
        }
      })
    } else {
      this.setState({
        formContact: {
          ...this.state.formContact,
          contacts
        }
      })    
    }
  }

  handleOpen = (e, object, index) => {
    e.preventDefault()
    this.setState({
      formContact: {
        ...this.state.formContact,
        indexSelected: index,
        form: {...object},
        selected: true,
        validation: {}
      }
    })
  }

  handleOnClickCancel = (e) => {
      this.setState({   
          formContact: {
            ...this.state.formContact,
            selected: false,
            indexSelected: -1,
            form: {
              nom: '',
              telephone: '',
              email: ''
            },
            validation: {}
        }})
  }

  handleOnClickModif = (e) => {
    const resultat = valider(this.state.formContact.form, validationClient.formContact)
    if (resultat.reussite) {
      let contacts = this.state.formContact.contacts.slice()
      contacts[this.state.formContact.indexSelected] = ({...this.state.formContact.form})
      this.setState({   
          formContact: {
            contacts,
            selected: false,
            indexSelected: -1,
            form: {
              nom: '',
              telephone: '',
              email: ''
            },
            validation: {}
        }})
    } else {
      this.setState({
        formContact: {
          ...this.state.formContact,
          validation: resultat.validation 
        }
      })
    }
  }

  handleOnClickAdd = (e) => {
    const resultat = valider(this.state.formContact.form, validationClient.formContact)
    if (resultat.reussite) {
      let contacts = this.state.formContact.contacts.slice()
      contacts.push({...this.state.formContact.form})
      this.setState({      
          formContact: {
            contacts,
            form: {
              nom: '',
              telephone: '',
              email: ''
            },
            validation: {}
        }})
    } else {
      this.setState({
        formContact: {
          ...this.state.formContact,
          validation: resultat.validation 
        }
      })
    }
  }

  handlePrevStep = (e) => { this.setState({ step: this.state.step - 1 }) }

  handleNextStep = (e) => {
    e.preventDefault()
    const { step, formClient } = this.state
    switch (step) {
      case 0 :
        let resultat = valider(formClient.form, validationClient.formClient)
        if (resultat.reussite) {
          this.setState({ step: step + 1, formClient: {...this.state.formClient, validation: {}} })
        } else {
          this.setState({
            formClient: {
              ...this.state.formClient,
              validation: resultat.validation 
            }
          })
        }
        return
      case 1 :
        postClient({
          ...this.state.formClient.form,
          contacts: this.state.formContact.contacts
        }).then(this.props.history.push('/clients'))
        return
      default :
        return
    }
  }

  ActualStep = (step) => {
    const { formClient, formContact } = this.state
    const { referants } = this.props
    const FormClient = (
      <div>
        <Form
        {...formClient.form}
        validation={formClient.validation}
        modeSelection={true}
        referants={referants}
        selected={[formClient.form.categorie]}
        handleOnChange={this.handleOnChangeClient}
        handleOnClickCategorie={this.handleOnClickCategorie} />
        <PreverNexter handleNextStep={this.handleNextStep} />
      </div>)
    const FormContactsProps = (
      <div>
        <FormContacts
        form={formContact.form}
        contacts={formContact.contacts}
        editMode={true}
        handleOpen={this.handleOpen}
        handleDelete={this.handleDelete}
        validation={formContact.validation}
        handleOnClickCancel={this.handleOnClickCancel}
        handleOnClickModif={this.handleOnClickModif}
        handleOnClickAdd={this.handleOnClickAdd}
        selected={formContact.selected}
        handleOnChange={this.handleOnChangeContact} />
        <PreverNexter labelNext={'Ajouter le client'} handlePrevStep={this.handlePrevStep} handleNextStep={this.handleNextStep} />
      </div>)
    switch (step) {
      case 0 :
        return FormClient
      case 1 :
        return FormContactsProps
      default :
        return FormClient
    }
  }

  render() {
    if (this.props.isFetching) {
      return <Loader />
    }
    return (this.ActualStep(this.state.step));
  }
}

const mapStateToProps = state => {
  return {
    referants: state.client.referants,
    isFetching: state.client.isFetching
  }
}

const mapDispatchToProps = { getReferants: actions.getReferants }

Steps = connect(mapStateToProps, mapDispatchToProps)(Steps)

export default Steps;