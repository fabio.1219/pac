import React from 'react';
import Field from 'components/global/Field'

const FormContact = ({nom, telephone, email, nomButtonCancel, nomButtonAdd, validation, handleOnChange, handleOnClickCancel, handleOnClickAdd}) => {
  return (
    <div style={{marginTop: '2rem'}}>
      <h4 className="big-label">Informations du contact</h4>
    <div className="row" style={{marginBottom: '0'}}>
      <div className="column column-form-field">
        <Field label={'Nom et prénom'} validation={validation['nom']}>
          <input type="text" name="nom" value={nom} onChange={handleOnChange} />
        </Field>
      </div>
      <div className="column column-form-field">
        <Field label={'Numéro de téléphone'} validation={validation['telephone']}>
          <input type="text" name="telephone" value={telephone} onChange={handleOnChange} onkeypress="return event.charCode >= 48 && event.charCode <= 57" />
        </Field>
      </div>
      <div className="column column-form-field">
        <Field label={'Email'} validation={validation['email']}>
          <input type="text" name="email" value={email} onChange={handleOnChange} />
        </Field>
      </div>
    </div>
    <div className="row" style={{marginTop: '0'}}>
      {handleOnClickCancel && <button style={{marginTop: '1.1rem', marginRight:'0.5rem'}} className="simple-button-icon simple-button-icon--red" onClick={handleOnClickCancel}>{nomButtonCancel}</button>}
      {handleOnClickAdd && <button style={{marginTop: '1.1rem'}} className="simple-button-icon simple-button-icon--primary" onClick={handleOnClickAdd}>{nomButtonAdd}</button>}
    </div>
    </div>
  );
};

export default FormContact;