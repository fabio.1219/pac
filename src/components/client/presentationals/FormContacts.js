import React from 'react';
import Table from 'components/global/Table'
import FormContact from './FormContact'

const ENTETES = ['Nom', 'Numéro de téléphone', 'Email']
const KEYS = [contact => contact.nom, contact => contact.telephone, contact => contact.email]

const TableContacts = ({contacts, handleOpen, handleDelete}) => {
  const contactsLength = contacts.length
  if (contactsLength !== 0) {
    return (
      <div>
        <h4 className="big-label">Tous les contacts</h4>
        <Table entetes={ENTETES} keys={KEYS} rows={contacts} handleOpen={handleOpen} handleDelete={handleDelete} />
      </div>
    )
  } else {
    return (
      <div>
        <div className='notfound-p'><p>Aucun contact trouvé.</p></div>
      </div>
    )
  }
}

const CasesFormContact = ({contactsLength, editMode, selected, form, validation, handleOnChange, handleOnClickCancel, handleOnClickModif, handleOnClickAdd}) => {
  if (!editMode) {
    return (<div />)
  } else {
    if (selected) {
      return ( 
          <FormContact 
          {...form}
          validation={validation}
          nomButtonCancel={'Annuler'}
          nomButtonAdd={'Modifier le contact'}
          handleOnChange={handleOnChange}
          handleOnClickCancel={handleOnClickCancel}
          handleOnClickAdd={handleOnClickModif} />
      )
    } else if (contactsLength < 3) {
      return (
        <FormContact 
        {...form}
        validation={validation}
        handleOnChange={handleOnChange}
        nomButtonAdd={'Ajouter le contact'}
        handleOnClickAdd={handleOnClickAdd} />
      )
    }
  }
}

const FormContacts = ({contacts, editMode, handleOpen, handleDelete, selected, form, validation, handleOnChange, handleOnClickCancel, handleOnClickModif, handleOnClickAdd}) => {
  return (
    <div className="card-content">
      <h3>Contacts</h3>
      <TableContacts contacts={contacts} handleOpen={editMode && handleOpen} handleDelete={editMode && handleDelete} />
      <CasesFormContact 
        contactsLength={contacts.length}
        editMode={editMode}
        selected={selected}
        form={form}
        validation={validation}
        handleOnChange={handleOnChange}
        handleOnClickCancel={handleOnClickCancel}
        handleOnClickModif={handleOnClickModif}
        handleOnClickAdd={handleOnClickAdd} />
    </div>
  );
};

export default FormContacts;