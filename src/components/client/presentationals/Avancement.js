import React from 'react';

const Etape = ({icone, label}) => {
  return (
    <div className="avancement-etape">
      <i className={`fa fa-${icone}-o`}></i>
      <label>{label}</label>
    </div>
  )
}

const Avancement = () => {
  return (
    <div>
    <section className="card-content">
      <div className="form-avancement avancement">
        <Etape icone={'id-card'} label={'1. Informations'} />
        <div className="avancement-separateur" />
        <Etape icone={'address-book'} label={'2. Contacts'} />
      </div>
    </section>
    <div className="all-separateur" />
    </div>
  );
};

export default Avancement;