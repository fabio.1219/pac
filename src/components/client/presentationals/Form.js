import React from 'react'
import Field from 'components/global/Field'
import Select from 'components/global/Select'
import Categories from 'components/global/Categories'
import Text from 'components/global/Text'

const LABELS = ['Sans catégorie', '1', '2', '3', '4', '5', '6', '7', '8', '9']

const Form = ({
  nom,
  groupe,
  secteur,
  referant,
  referants,
  handleOnChange,
  modeSelection,
  selected,
  handleOnClickCategorie,
  validation,
  isDisabled
}) => {
  return (
    <div className='card-content'>
      <h3>Informations du client</h3>
      <div className='form row'>
        <div className='form-column'>
          <Field label={'Client'} validation={validation['nom']}>
            {isDisabled
              ? <Text>{nom}</Text>
              : <input
                type='text'
                name='nom'
                value={nom}
                onChange={handleOnChange}
                />}
          </Field>
          <Field label={'Groupe'} validation={validation['groupe']}>
            {isDisabled
              ? <Text>{groupe}</Text>
              : <input
                name='groupe'
                type='text'
                value={groupe}
                onChange={handleOnChange}
                />}
          </Field>
          <Field label={'Secteur'} validation={validation['secteur']}>
            {isDisabled
              ? <Text>{secteur}</Text>
              : <input
                name='secteur'
                type='text'
                value={secteur}
                onChange={handleOnChange}
                />}
          </Field>
          <Field label={'Notre référant'} validation={validation['referant']}>
            {isDisabled
                ? <Text>{referant && referant.nom}</Text>
                : <div className='form-select'>
              <Select
                  name='referant'
                  value={referant.id}
                  options={referants}
                  onChange={handleOnChange}
                  >
                  <option value=''>Choisir un référant...</option>
                  {referants.map((referant, index) => (
                    <option value={referant.id} key={referant.id}>{referant.nom}</option>
                    ))}
                </Select>
            </div>}
          </Field>
        </div>
        <div className='form-column'>
          <Field label={'Catégorie du client'} validation={validation['categorie']}>
            <Categories
              labels={LABELS}
              isDisabled={isDisabled}
              modeSelection={modeSelection}
              selected={selected}
              handleOnClick={handleOnClickCategorie}
            />
          </Field>
        </div>
      </div>
    </div>
  )
}

export default Form
