import React from 'react'
import Field from 'components/global/Field'
import { formateDateComptoDate } from 'lib/DateFormat'

const Compterendu = ({ date, utilisateur, texte }) => {
  return (
    <div className='compterendu'>
      <div className='title'>Compte-rendu du {formateDateComptoDate(date)} par {utilisateur}</div>
      <div className='content'>{`${texte}`}</div>
    </div>
  )
}

const Comptesrendus = ({ comptesrendus, compterendu, handleOnChange, handleOnClick }) => {
  return (
    <div>
      <div className='card-content'>
        <h3>Comptes-rendus</h3>
        <div className='comptesrendus'>
          {comptesrendus.length > 0 
          ? 
          comptesrendus.map(compterendu => (
            <Compterendu
              key={compterendu.id}
              date={compterendu.created_at}
              utilisateur={compterendu.utilisateur.nom}
              texte={compterendu.texte}
            />
          ))
          : <div className='notfound-p'><p>Aucun compte-rendu trouvé.</p></div>}
        </div>
      </div>
      <div className='all-separateur' />
      <div className='card-content'>
        <Field label='Nouveau compte-rendu'>
          <textarea
            className='noresize'
            rows={8}
            name='compterendu'
            value={compterendu}
            onChange={handleOnChange}
          />
        </Field>
        <button className='button' onClick={handleOnClick} disabled={compterendu.length === 0}>
          Publier le compte-rendu
        </button>
      </div>
    </div>
  )
}

export default Comptesrendus
