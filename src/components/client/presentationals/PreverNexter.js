import React from 'react'

const PreverNexter = ({
  handlePrevStep,
  handleNextStep,
  labelPrev = 'Précédent',
  labelNext = 'Suivant'
}) => {
  return (
    <div>
      <div className='all-separateur' />
      <section className='card-content container-2-elements'>
        <div className='container-2-elements--first'>
          {handlePrevStep &&
            <button
              name='prev'
              className='simple-button-icon simple-button-icon--primary'
              onClick={handlePrevStep}
            >
              {labelPrev}
            </button>}
        </div>
        <div className='container-2-elements--second'>
          {handleNextStep &&
            <button name='next' className='button' onClick={handleNextStep}>{labelNext}</button>}
        </div>
      </section>
    </div>
  )
}

export default PreverNexter
