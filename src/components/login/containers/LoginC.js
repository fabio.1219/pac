import FormLogin from '../presentationals/FormLogin'
import React, { Component } from 'react'

import Loader from 'components/global/loader/Loader'

import FormHandlerHOC from 'components/global/FormHandlerHOC'
import { connect } from 'react-redux'
import { actions } from 'redux/login'

class LoginC extends Component {
  state = {
    login: '',
    password: '',
    cookie: false
  }

  handleOnChange = ({ change }) => {
    this.setState({ ...change })
  }

  FormHandled = FormHandlerHOC(this.handleOnChange, 0)(FormLogin)

  handleOnClickLogin = e => {
    e.preventDefault()
    const { login, password, cookie } = this.state
    if (cookie) {
      this.props.saveInCookie()
    }
    this.props.login({ login, password })
  }

  render () {
    const disabled = this.state.login.length === 0 || this.state.password.length === 0
    const FormHandled = this.FormHandled
    const error = this.props.error
    if (this.props.isFetching) {
      return <div className='card card-login login'><Loader /></div>
    }

    return (
      <div>
        <FormHandled
          form={this.state}
          disabled={disabled}
          error={error}
          handleOnClickLogin={this.handleOnClickLogin}
        />
      </div>
    )
  }
}

const mapStateToProps = state => {
  return {
    isFetching: state.login.isFetching,
    error: state.login.error
  }
}

const mapDispatchToProps = { login: actions.login, saveInCookie: actions.saveInCookie }

LoginC = connect(mapStateToProps, mapDispatchToProps)(LoginC)

export default LoginC
