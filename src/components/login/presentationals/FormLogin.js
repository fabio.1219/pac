import React from 'react'
import logo2 from '../../../assets/logo2.png'

const FormLogin = ({
  login,
  password,
  cookie,
  disabled,
  error,
  handleOnChange,
  handleOnClickLogin
}) => {
  return (
    <div className='card card-login login'>
      <div className='login-image'>
        <img src={logo2} alt='' />
      </div>
      <h2>Bienvenue !</h2>
      <form className='form'>
        <div className='form-field'>
          <label>Nom d'utilisateur</label>
          <input type='text' name='login' value={login} onChange={handleOnChange} />
        </div>
        <div className='form-field'>
          <label>Mot de passe</label>
          <input type='password' name='password' value={password} onChange={handleOnChange} />
        </div>
        <div className='form-checkbox'>
          <input type='checkbox' name='cookie' checked={cookie} onChange={handleOnChange} />
          Rester connecté
        </div>
        <div className='form-field--error' style={{ marginBottom: '1rem' }}>
          {error}
        </div>
        <input
          type='submit'
          className={'button' + (disabled ? ' disabled' : '')}
          style={{ width: '100%' }}
          onClick={handleOnClickLogin}
          value='Connexion'
        />
      </form>
    </div>
  )
}

/**         <div className='notification notification--error'>
          <button className='notification-close' />
          Connexion échoué
        </div>
 */

export default FormLogin
