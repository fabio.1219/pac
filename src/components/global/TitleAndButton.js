import React from 'react'

const TitleAndButton = ({ title, children }) => {
  return (
    <div className='container-2-elements'>
      <h3 className='container-2-elements--first align-to-button--input'>{title}</h3>
      <div className='container-2-elements--second'>{children}</div>
    </div>
  )
}

export default TitleAndButton
