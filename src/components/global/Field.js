import React from 'react'

const Field = ({ label, validation, children, isDisabled = false }) => {
  return (
    <div className={`form-field${!isDisabled ? '' : ' form-field--disabled'}`}>
      <label>{label}</label>
      {children}
      <div className='form-field--error'>{validation && validation}</div>
    </div>
  )
}

export default Field
