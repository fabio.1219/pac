import React from 'react';

const Pagination = ({ prev_page_url, next_page_url, current_page, pages, handleOnClick }) => {
  let disabledPrev = prev_page_url ? '' : ' page--disabled'
  let disabledNext = next_page_url ? '' : ' page--disabled'
  return (
    <nav className="page">
      <a className={'page-previous' + disabledPrev} name={prev_page_url} onClick={handleOnClick}>Précédent</a>
      <ul>
        {pages.map(page =>
          <li key={page}>
            <a className={'page' + (page === current_page && '--active')} name={page} onClick={handleOnClick}>{page}</a>
          </li>
        )}
      </ul>
      <a className={'page-next' + disabledNext} name={next_page_url} onClick={handleOnClick}>Suivant</a>
    </nav>
  );
};

export default Pagination;