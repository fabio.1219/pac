import React, { Component } from 'react';
import Pagination from './Pagination'
import PropTypes from 'prop-types';

class PaginationWithArray extends Component {
  static propTypes = {
    prev_page_url: PropTypes.string,
    next_page_url: PropTypes.string,
    current_page: PropTypes.number.isRequired,
    last_page: PropTypes.number.isRequired,
    handleOnClick: PropTypes.func.isRequired
  }


  arrayOfPages = (current, last_page) => {
    let cases = []
    if (+last_page < 5) { // cas en dessous de 5 pages
      for (let i = 0; i < +last_page; i++) { cases[i] = i+1 }
    } else { // cas 5+ pages  
      let caseMilieu = +current
      if (caseMilieu < 3) { caseMilieu = 3 }
      if (caseMilieu > last_page - 2) { caseMilieu = last_page - 2 }
      for (let i = 0; i < 5; i++) { cases[i] = caseMilieu - 2 + i }
    }
    return cases
  }

  render() {
    let { current_page, last_page } = this.props
    return (
      <Pagination pages={this.arrayOfPages(current_page, last_page)} {...this.props} />
    );
  }
}

export default PaginationWithArray;