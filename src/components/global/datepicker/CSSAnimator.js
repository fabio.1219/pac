import React, { Component } from 'react'

class CSSAnimator extends Component {
  state = {
    children: this.props.children
  }

  componentWillReceiveProps (nextProps) {
    const children = this.state.children
    const nextChildren = nextProps.children
    if (children.props.children !== nextChildren.props.children) {
      const newClassName = nextProps.children.props.className + ' paschangement'
      this.setState({
        children: React.cloneElement(nextChildren, {
          ...nextChildren.props,
          style: { backgroundColor: 'red' }
        })
      })
    }
  }

  render () {
    const { children } = this.state
    return (
      <div>
        {children}
      </div>
    )
  }
}

export default CSSAnimator
