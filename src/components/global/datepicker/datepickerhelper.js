const days_in_month = [31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31]

const getTodayForComparison = () => {
  const currentDate = new Date()
  let day = currentDate.getDate()
  let month = currentDate.getMonth() + 1
  const year = currentDate.getFullYear()
  if (day < 10) {
    day = `0${day}`
  }
  if (month < 10) {
    month = `0${month}`
  }
  return `${year}-${month}-${day}`
}

const getFirstDayOfTheWeek = (month, year) => {
  // 0 = dimanche, 1 = lundi, 2 = mardi, etc..
  const firstDay = new Date(year, month, 1)
  const dayOfWeek = firstDay.getDay() - 1 // pour mettre lundi à la première case du tableau
  return dayOfWeek !== -1 ? dayOfWeek : 6 // gérer le cas du dimanche
}

const getNumberOfDaysInTheMonth = (month, year) => {
  if (month === 1) {
    // cas du février année bissexstile
    if ((year % 4 === 0 && year % 100 !== 0) || year % 400 === 0) {
      return 29
    }
  }
  return days_in_month[month]
}

const getWeeksOfTheMonth = (month, year) => {
  const firstDay = getFirstDayOfTheWeek(month, year)
  const totalDays = firstDay + getNumberOfDaysInTheMonth(month, year)
  return Math.ceil(totalDays / 7)
}

const getArrayOfDays = (month, year) => {
  let days = []
  const firstDay = getFirstDayOfTheWeek(month, year)
  const finalDay = getNumberOfDaysInTheMonth(month, year)
  for (let i = 0; i < firstDay; i++) {
    days.push('')
  }
  for (let i = 1; i <= finalDay; i++) {
    days.push(i)
  }
  const daysMissingToFillAWeek = 7 - days.length % 7
  for (let i = 0; i < daysMissingToFillAWeek; i++) {
    days.push('')
  }
  return days
}

const getArrayOfWeeks = (month, year) => {
  let weeks = []
  let days = getArrayOfDays(month, year)
  const numberOfWeeks = getWeeksOfTheMonth(month, year)
  for (let i = 0; i < numberOfWeeks; i++) {
    const beginToSlice = 7 * i
    const lastToSlice = beginToSlice + 7
    weeks.push(days.slice(beginToSlice, lastToSlice))
  }
  return weeks
}

const formatDate = (day, month, year) => {
  let fDay = day, fMonth = month, fYear = year
  if (fDay < 10) {
    fDay = `0${fDay}`
  }
  if (fMonth < 10) {
    fMonth = `0${fMonth}`
  }
  return `${fDay}.${fMonth}.${fYear}`
}

const formatDateForComparison = (day, month, year) => {
  let fDay = day, fMonth = month + 1, fYear = year
  if (fDay < 10) {
    fDay = `0${fDay}`
  }
  if (fMonth < 10) {
    fMonth = `0${fMonth}`
  }
  return `${fYear}-${fMonth}-${fDay}`
}

export { getTodayForComparison, getArrayOfWeeks, formatDate, formatDateForComparison }
