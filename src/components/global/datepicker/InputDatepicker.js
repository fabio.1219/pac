import React, { Component } from 'react'
import Datepicker from './Datepicker'

class InputDatepicker extends Component {
  state = {
    showDatepicker: false
  }

  style = {
    position: 'absolute',
    top: '100%',
    zIndex: '100'
  }

  handleOnFocus = e => {
    this.setState({
      showDatepicker: true
    })
  }

  handleOnBlur = e => {
    this.setState({
      showDatepicker: false
    })
  }

  stopPropagation = e => {
    e.stopPropagation()
    e.nativeEvent.stopImmediatePropagation()
  }

  handleOnClickClear = e => {
    e.stopPropagation()
    e.nativeEvent.stopImmediatePropagation()
    this.props.handleOnChange({}) // pour remettre à 0 la date
  }

  render () {
    const { showDatepicker } = this.state
    const { selectedDate = {}, handleOnChange } = this.props
    return (
      <div
        onBlur={this.handleOnBlur}
        style={{ position: 'relative', outline: 'none' }}
        tabIndex='0'
      >
        <div className='icon-input'>
          <input
            type='text'
            onFocus={this.handleOnFocus}
            onBlur={this.stopPropagation}
            value={selectedDate.formatDate || ''}
            style={{ pointerEvents: 'all' }}
            readOnly
          />
          {selectedDate.formatDate &&
            <i
              className='fa fa-times-circle clickable'
              aria-hidden='true'
              style={{ zIndex: 99 }}
              onClick={this.handleOnClickClear}
            />}
        </div>

        {showDatepicker &&
          <div style={this.style}>
            <Datepicker selectedDate={selectedDate} handleOnChange={handleOnChange} />
          </div>}
      </div>
    )
  }
}

export default InputDatepicker
