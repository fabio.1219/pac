// logic from http://jszen.blogspot.ch/2007/03/how-to-build-simple-calendar-with.html
import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { getTodayForComparison, getArrayOfWeeks, formatDate, formatDateForComparison } from './datepickerhelper'
import Day from './Day'
import './datepicker.css'

class Datepicker extends Component {
  static propTypes = {
    selectedDate: PropTypes.object,
    handleOnChange: PropTypes.func
  }

  static defaultProps = {
    daysLabels: ['Lun', 'Mar', 'Mer', 'Jeu', 'Ven', 'Sam', 'Dim'],
    monthsLabels: [
      'Janvier',
      'Février',
      'Mars',
      'Avril',
      'Mai',
      'Juin',
      'Juillet',
      'Août',
      'Septembre',
      'Octobre',
      'Novembre',
      'Décembre'
    ],
    selectedDate: {},
    currentDate: new Date()
  }

  state = {
    month: this.props.currentDate.getMonth(),
    year: this.props.currentDate.getFullYear()
  }

  onClickPrevMonth = () => {
    const { month, year } = this.state
    const date = new Date(year, month - 1, 1)
    this.setState({
      month: date.getMonth(),
      year: date.getFullYear()
    })
  }

  onClickNextMonth = () => {
    const { month, year } = this.state
    const date = new Date(year, month + 1, 1)
    this.setState({
      month: date.getMonth(),
      year: date.getFullYear()
    })
  }

  getHeaderMonthYear = () => {
    const { month, year } = this.state
    const monthName = this.props.monthsLabels[month]
    return `${monthName} ${year}`
  }

  renderHeaderWeekDays () {
    return this.props.daysLabels.map((day, index) => (
      <div className='calendar-dayweek-days' key={index}>
        {day}
      </div>
    ))
  }

  handleOnClickDays = date => {
    const realDate = {
      ...date,
      formatDate: formatDate(date.day, date.month + 1, date.year),
      monthName: this.props.monthsLabels[date.month]
    }
    this.props.handleOnChange(realDate)
  }

  isSelected = date => {
    const selectedDate = this.props.selectedDate
    return (
      date.day === selectedDate.day &&
      date.month === selectedDate.month &&
      date.year === selectedDate.year
    )
  }

  isBeforeToday = date => {
    const today = getTodayForComparison()
    const dateFormat = formatDateForComparison(date.day, date.month, date.year)
    return dateFormat < today
  }

  renderDays () {
    const { month, year } = this.state
    const monthyear = { month, year }
    const weeks = getArrayOfWeeks(month, year)
    return weeks.map((week, index) => (
      <div className='calendar-body-weeks' key={index}>
        {week.map((day, index) => {
          if (day !== '') {
            const date = { ...monthyear, day }
            const selected = this.isSelected(date)
            const disabled = this.isBeforeToday(date)
            return (
              <Day
                date={date}
                key={`${index}${day}`}
                selected={selected}
                disabled={disabled}
                handleOnClick={this.handleOnClickDays}
              />
            )
          } else {
            return <div className='calendar-body-day' key={`${index}${day}`}>{day}</div>
          }
        })}
      </div>
    ))
  }

  render () {
    const { className = '' } = this.props
    return (
      <div className={`datepicker ${className}`}>
        <nav className='datepicker-header'>
          <div className='btn datepicker-header--prev' onClick={this.onClickPrevMonth} />
          <div className='datepicker-header--title'>{this.getHeaderMonthYear()}</div>
          <div className='btn datepicker-header--next' onClick={this.onClickNextMonth} />
        </nav>
        <section className='calendar'>
          <header className='calendar-dayweek'>
            {this.renderHeaderWeekDays()}
          </header>
          <section className='calendar-body'>
            {this.renderDays()}
          </section>
        </section>
      </div>
    )
  }
}

export default Datepicker
