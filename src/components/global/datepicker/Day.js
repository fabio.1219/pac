import React, { PureComponent } from 'react'

class Day extends PureComponent {
  date = this.props.date

  onClick = () => {
    this.props.handleOnClick(this.date)
  }

  render () {
    const { date, selected, disabled } = this.props
    const className =
      'calendar-body-day calendar-body-day--selectable' +
      (!selected ? '' : ' calendar-body-day--selected') +
      (!disabled ? '' : ' calendar-body-day--disabled')
    return (
      <div className={className} onClick={this.onClick}>
        {date.day}
      </div>
    )
  }
}

export default Day
