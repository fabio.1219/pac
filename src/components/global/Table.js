import React, { Component } from 'react'
import PropTypes from 'prop-types'

class Supprimer extends Component {
  handleOnClick = e => {
    const { object, index, handleOnClick } = this.props
    handleOnClick(e, object, index)
  }

  render () {
    return (
      <button
        className='simple-button-icon simple-button-icon--table simple-button-icon--red'
        style={{ marginTop: '0.11rem' }}
        onClick={this.handleOnClick}
      >
        <i className='fa fa fa-times' aria-hidden='true' />
      </button>
    )
  }
}

class Ouvrir extends Component {
  handleOnClick = e => {
    const { object, index, handleOnClick } = this.props
    handleOnClick(e, object, index)
  }

  render () {
    return (
      <button
        className='simple-button-icon simple-button-icon--table simple-button-icon--primary'
        style={{ marginTop: '0.11rem' }}
        onClick={this.handleOnClick}
      >
        <i className='fa fa-external-link' aria-hidden='true' />
      </button>
    )
  }
}

const Entetes = ({ entetes, lastColumn }) => {
  return (
    <thead>
      <tr className='center'>
        {entetes.map((entete, index) => <th key={index}>{entete}</th>)}
        {lastColumn && <th className='right' />}
      </tr>
    </thead>
  )
}

const Rows = ({ rows, keys, handleOpen, handleDelete }) => {
  return (
    <tbody>
      {rows.map((row, index) => (
        <tr className='center' key={row.id}>
          {keys.map((key, index) => (
            <td key={index}>
              {key(row)}
            </td>
          ))}
          {(handleOpen || handleDelete) &&
            <td className='right'>
              {handleOpen && <Ouvrir object={row} index={index} handleOnClick={handleOpen} />}
              {handleDelete &&
                <Supprimer object={row} index={index} handleOnClick={handleDelete} />}
            </td>}
        </tr>
      ))}
    </tbody>
  )
}

const Table = ({ entetes, rows, keys, handleOpen, handleDelete }) => {
  return (
    <table className='table'>
      <Entetes entetes={entetes} lastColumn={handleOpen || handleDelete} />
      <Rows rows={rows} keys={keys} handleOpen={handleOpen} handleDelete={handleDelete} />
    </table>
  )
}

Table.propTypes = {
  entetes: PropTypes.array.isRequired,
  rows: PropTypes.array.isRequired
}

export default Table
