import React, { Component } from 'react'

class Modal extends Component {
  stopPropagation = e => {
    e.stopPropagation()
  }

  render () {
    const { handleOnClickClose, children } = this.props
    return (
      <div className='modal' onClick={handleOnClickClose}>
        <div className='modal-content' onClick={this.stopPropagation}>{children}</div>
      </div>
    )
  }
}

export default Modal
