import React, { Component } from 'react'
import Tabs from './Tabs'

function TabsHOC (TabsToDisplay, actionUpdate) {
  return class TabsHOC extends Component {
    handleOnClickTabs = e => {
      actionUpdate({ etatTab: e.target.name })
    }

    renderToDisplay = () => {
      const toDisplay = TabsToDisplay.find(tab => tab.nom === this.props.etat)
      return toDisplay ? toDisplay.component : TabsToDisplay[0].component
    }

    render () {
      const ToDisplay = this.renderToDisplay()
      return (
        <div>
          <Tabs
            tabs={TabsToDisplay}
            selected={this.props.etat}
            handleOnClick={this.handleOnClickTabs}
          />
          <ToDisplay history={this.props.history} />
        </div>
      )
    }
  }
}

export default TabsHOC
