import React, { Component } from 'react'

class ModalSupprimer extends Component {
  stopPropagation = e => {
    e.stopPropagation()
  }

  render () {
    const { handleOnClickAnnuler, handleOnClickSupprimer } = this.props
    return (
      <div className='modal' onClick={handleOnClickAnnuler}>
        <div className='modal-content' onClick={this.stopPropagation}>
          <div className='card'>
            <h2 className='card-header'>Confirmer la suppression.</h2>
            <div className='card-content'>
              <button className='button' onClick={handleOnClickSupprimer}>Supprimer</button>
              <button
                className='simple-button-icon simple-button-icon--red'
                onClick={handleOnClickAnnuler}
              >
                Annuler
              </button>
            </div>
          </div>
        </div>
      </div>
    )
  }
}

export default ModalSupprimer
