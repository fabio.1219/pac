import React from 'react';

const Tab = ({nom, nomFormat, handleOnClick, isSelected}) => {
  const classSelected = isSelected ? '--active' : ''
  return (
    <li className={classSelected}>
      <a name={nom} onClick={handleOnClick}>{nomFormat}</a>
    </li>
  )
}

const Tabs = ({tabs, selected, handleOnClick}) => {
  return (
    <div className="tabs ajust">
      <ul>
      {tabs.map(tab => 
        <Tab nom={tab.nom} key={tab.nom} isSelected={tab.nom === selected} nomFormat={tab.nomFormat} handleOnClick={handleOnClick} />
      )}
      </ul>
    </div>
  );
};

export default Tabs;