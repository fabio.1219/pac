import React from 'react'

const Text = ({ children }) => {
  return (
    <div className='form-field--text'>
      {children}
    </div>
  )
}

export default Text
