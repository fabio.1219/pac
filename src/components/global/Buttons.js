import React from 'react';

const Buttons = ({buttons}) => {
  return (
    <div>
        {buttons.map((button, index) =>
          <button className="button" key={index} onClick={button.handleOnClick}>{button.label}</button>
        )}
    </div>
  );
};

export default Buttons;