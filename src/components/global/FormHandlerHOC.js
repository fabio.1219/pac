import React from 'react'

function FormHandlerHOC (actionUpdate, delay = 600) {
  return function (Component) {
    return class FormHandler extends React.Component {
      constructor (props) {
        super(props)
        this.timeoutID = {} // contiens les ID des timeout pour empecher si un changement se fait trop vite
        this.state = {
          ...props.form
        }
      }

      handleOnChange = e => {
        const target = e.target
        const value = target.type === 'checkbox' ? target.checked : target.value
        const name = target.name

        clearTimeout(this.timeoutID[name]) // empecher le changement de se faire si deja arrivé
        const change = { [name]: value }
        const updateStore = change =>
          (this.timeoutID[name] = setTimeout(() => {
            actionUpdate({ change })
          }, delay)) // laisser une latence avant update store
        this.setState({ ...change }, () => updateStore(change))
      }

      componentWillReceiveProps (nextProps) {
        if (JSON.stringify(this.props.form) !== JSON.stringify(nextProps.form)) {
          this.setState({
            ...nextProps.form
          })
        }
      }

      render () {
        const form = { ...this.props.form, ...this.state } // permet que le state local gagne par rapport au prop venant du store
        const cleanProps = { ...this.props, ...form, form: undefined }
        return <Component {...cleanProps} handleOnChange={this.handleOnChange} />
      }
    }
  }
}

export default FormHandlerHOC
