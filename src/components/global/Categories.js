import React from 'react'
import PropTypes from 'prop-types'

const ButtonCategorie = ({ label, value, typeClient, isSelected, handleOnClick }) => {
  let selected = isSelected ? `categories-button--${typeClient}--active` : ''
  return (
    <button
      className={`categories-button categories-button--${typeClient} ${selected}`}
      name={'categorie'}
      value={value}
      onClick={handleOnClick}
    >
      {label}
    </button>
  )
}

const Categories = ({
  labels,
  modeSelection,
  isDisabled = false,
  selected,
  showSansCategorie = true,
  handleOnClick
}) => {
  return (
    <div className={`categories ${isDisabled ? 'categories-noclick' : ''}`}>
      <div className='row'>
        <div className='column connaissance' />
        <div className='column' />
        <div className='column'><b>Potentiel</b></div>
        <div className='column' />
      </div>
      <div className='row potentiel'>
        <div className='column connaissance '/>
        <div className='column'>Faible</div>
        <div className='column'>Moyen</div>
        <div className='column'>Fort</div>
      </div>
      <div className='row row-button'>
        <div className='column connaissance'>Elevée</div>
        <div className='column'>
          <ButtonCategorie
            label={labels[7]}
            typeClient='middle'
            value={7}
            isSelected={modeSelection && selected.includes(7)}
            handleOnClick={handleOnClick}
          />
        </div>
        <div className='column'>
          <ButtonCategorie
            label={labels[8]}
            typeClient='top'
            value={8}
            isSelected={modeSelection && selected.includes(8)}
            handleOnClick={handleOnClick}
          />
        </div>
        <div className='column'>
          <ButtonCategorie
            label={labels[9]}
            typeClient='top'
            value={9}
            isSelected={modeSelection && selected.includes(9)}
            handleOnClick={handleOnClick}
          />
        </div>
      </div>
      <div className='row row-button'>
        <div className='column connaissance'>Moyenne</div>
        <div className='column'>
          <ButtonCategorie
            label={labels[4]}
            typeClient='bottom'
            value={4}
            isSelected={modeSelection && selected.includes(4)}
            handleOnClick={handleOnClick}
          />
        </div>
        <div className='column'>
          <ButtonCategorie
            label={labels[5]}
            typeClient='middle'
            value={5}
            isSelected={modeSelection && selected.includes(5)}
            handleOnClick={handleOnClick}
          />
        </div>
        <div className='column'>
          <ButtonCategorie
            label={labels[6]}
            typeClient='top'
            value={6}
            isSelected={modeSelection && selected.includes(6)}
            handleOnClick={handleOnClick}
          />
        </div>
      </div>
      <div className='row row-button'>
        <div className='column connaissance'>Basse</div>
        <div className='column'>
          <ButtonCategorie
            label={labels[1]}
            typeClient='bottom'
            value={1}
            isSelected={modeSelection && selected.includes(1)}
            handleOnClick={handleOnClick}
          />
        </div>
        <div className='column'>
          <ButtonCategorie
            label={labels[2]}
            typeClient='bottom'
            value={2}
            isSelected={modeSelection && selected.includes(2)}
            handleOnClick={handleOnClick}
          />
        </div>
        <div className='column'>
          <ButtonCategorie
            label={labels[3]}
            typeClient='middle'
            value={3}
            isSelected={modeSelection && selected.includes(3)}
            handleOnClick={handleOnClick}
          />
        </div>
      </div>
      <div className='connaissance-label'><b>Connaissance</b></div>
      {showSansCategorie &&
        <div>
          <div className='row row-button'>
            <div className='column _25 connaissance' />
            <div className='column _75'><div className='categories-separateur' /></div>
          </div>
          <div className='row row-button'>
            <div className='column _25 connaissance' />
            <div className='column _75'>
              <ButtonCategorie
                label={labels[0]}
                typeClient='normal'
                value={0}
                isSelected={modeSelection && selected.includes(0)}
                handleOnClick={handleOnClick}
              />
            </div>
          </div>
        </div>}
    </div>
  )
}

Categories.propTypes = {
  labels: PropTypes.array.isRequired,
  handleOnClick: PropTypes.func.isRequired,
  modeSelection: PropTypes.bool.isRequired
}

export default Categories
