import React, { Component } from 'react'

class Select extends Component {
  handleOnChange = e => {
    const { name, onChange, options } = this.props
    const { selectedIndex, value } = e.target
    const realValue = selectedIndex > 0 ? options[selectedIndex - 1] : value
    onChange({
      target: {
        name,
        value: realValue
      }
    })
  }

  render () {
    const { name, value, children } = this.props
    return (
      <div className='form-select'>
        <select name={name} value={value} onChange={this.handleOnChange}>
          {children}
        </select>
      </div>
    )
  }
}

export default Select
