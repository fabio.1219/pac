import React, { Component } from 'react'
import './Loader.css'

class Loader extends Component {
  getAnimationStyle = () => {
    const random = top => Math.random() * top
    const animationDuration = `${random(100) / 100 + 0.6}s`
    const animationDelay = `${random(100) / 100 - 0.2}s`
    return {
      animation: `pulse ${animationDuration} ${animationDelay} infinite ease`,
      animationFillMode: 'both'
    }
  }

  render () {
    const loading = this.props.loading === undefined ? true : this.props.loading
    if (loading) {
      return (
        <div className='padding'>
          <div className='loading'>
            <div>
              <div className='loading-ball' style={this.getAnimationStyle()} />
              <div className='loading-ball' style={this.getAnimationStyle()} />
              <div className='loading-ball' style={this.getAnimationStyle()} />
            </div>

            <div>
              <div className='loading-ball' style={this.getAnimationStyle()} />
              <div className='loading-ball' style={this.getAnimationStyle()} />
              <div className='loading-ball' style={this.getAnimationStyle()} />
            </div>

            <div>
              <div className='loading-ball' style={this.getAnimationStyle()} />
              <div className='loading-ball' style={this.getAnimationStyle()} />
              <div className='loading-ball' style={this.getAnimationStyle()} />
            </div>
            <div className='loading-text'>
              En cours de chargement.
            </div>
          </div>
        </div>
      )
    }
    return null
  }
}

export default Loader
