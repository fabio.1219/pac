import React from 'react'
import MenuC from './menu/MenuC'
import Clients from './pages/Clients'
import AjouterClient from './pages/AjouterClient'
import AfficherClient from './pages/AfficherClient'
import Utilisateurs from './pages/Utilisateurs'
import AfficherUtilisateur from './pages/AfficherUtilisateur'
import AjouterUtilisateur from './pages/AjouterUtilisateur'
import Login from './pages/Login'
import ClientsCategories from './pages/ClientsCategories'
import TypesAction from './pages/TypesAction'
import CreerAction from './pages/CreerAction'
import AfficherActions from './pages/AfficherActions'
import AfficherAction from './pages/AfficherAction'
import Statistiques from './pages/Statistiques'
import ChangeMDP from './pages/ChangeMDP'
import Accueil from './pages/Accueil'

import { RESPONSABLE_SAISIE } from 'conf/typeUtilisateur'
import { connect } from 'react-redux'
import { withRouter } from 'react-router'
import { Switch, Route, Redirect } from 'react-router-dom'

let PrivateRoute = ({ component: Component, typeUtilisateur, authorization = RESPONSABLE_SAISIE,  ...rest }) => {
  return <Route {...rest} render={props => (
    typeUtilisateur <= authorization ? (
      <Component {...props}/>
    ) : (
      <Redirect to='/' />
    )
  )}/>
}

const mapStateToPropsPrivateRoute = state => {
  return {
    typeUtilisateur: state.login.decoded.type
  }
}

PrivateRoute = connect(mapStateToPropsPrivateRoute, null)(PrivateRoute)

const App = ({isLogged, changemdp, type}) => {
    if (isLogged) {
      if (changemdp) {
        return (
          <Switch>
            <Route path='/login/password' exact component={ChangeMDP} />
            <Redirect from='/' to='/login/password' />
          </Switch>
        )
      }
      return (
        <div>
          <MenuC />
          <main className='container'>
            <Switch>
              <PrivateRoute path='/clients' exact component={Clients} />
              <PrivateRoute path='/clients/ajouter' exact component={AjouterClient} />
              <Route path='/clients/:id' component={AfficherClient} />
              <PrivateRoute path='/utilisateurs' exact component={Utilisateurs} />
              <PrivateRoute path='/utilisateurs/ajouter' exact component={AjouterUtilisateur} />
              <PrivateRoute path='/utilisateurs/:id' component={AfficherUtilisateur} />
              <Route path='/categories' component={ClientsCategories} />
              <PrivateRoute path='/typesactions' component={TypesAction} />
              <Route path='/actions' exact component={AfficherActions} />
              <Route path='/actions/:id' exact component={AfficherAction} />
              <Route path='/actions/entreprendre/:id' component={CreerAction} />
              <PrivateRoute path='/statistiques' exact component={Statistiques} />
              <Route path='/' exact component={Accueil} />
              <Route path='/' render={() => <Redirect to='/' />} />
            </Switch>
          </main>
        </div>
      )
    } else {
      return (
        <div>
          <Switch>
            <Route path='/login' exact component={Login} />
            <Redirect from='/' to='/login' />
          </Switch>
        </div>
      )
    }
}

const mapStateToProps = state => {
  return {
    isLogged: state.login.isLogged,
    changemdp: state.login.decoded.changemdp
  }
}

export default withRouter(connect(mapStateToProps)(App))
