import React, { Component } from 'react'
import { connect } from 'react-redux'
import Table from 'components/global/Table'
import Pagination from 'components/global/pagination'
import { actions } from 'redux/actions'
import { actions as actionsAction } from 'redux/action'
import Loader from 'components/global/loader/Loader'

const ENTETES = ['Client', "Type d'action", "Date d'échéance", 'Date prévue', 'Etat']
const KEYS = [
  action => action.client.nom,
  action => action.typeaction.nom,
  action => action.dateEcheance,
  action => action.datePrevue || 'Pas de date prévue',
  action => action.etatActuel.etat.nom
]

class TableActions extends Component {
  handleOnClick = e => {
    e.preventDefault()
    this.props.getActions({ page: e.target.name[e.target.name.length - 1] })
  }

  componentWillMount () {
    this.props.getActions()
  }

  handleOpen = (e, object, id) => {
    this.props.openAction({ action: object })
    this.props.history.push(`/actions/${object.id}`)
  }

  render () {
    let {
      isFetching,
      from,
      to,
      total,
      current_page,
      last_page,
      prev_page_url,
      next_page_url,
      data
    } = this.props.actions
    if (!isFetching && data.length > 0) {
      return (
        <div className='card-content'>
          <div className='table-info'>
            <div className='table-info-no'>{from} - {to}</div>
            <div className='table-info-text'> actions sur </div>
            <div className='table-info-no'>{total}</div>
          </div>
          <Table entetes={ENTETES} rows={data} keys={KEYS} handleOpen={this.handleOpen} />
          <Pagination
            prev_page_url={prev_page_url}
            next_page_url={next_page_url}
            current_page={current_page}
            last_page={last_page}
            handleOnClick={this.handleOnClick}
          />
        </div>
      )
    } else if (isFetching) {
      return <Loader />
    } else {
      return <div className='card-content notfound-p'><p>Aucune action trouvée.</p></div>
    }
  }
}

const mapState = state => {
  return {
    actions: state.actions
  }
}

const mapDispatch = { getActions: actions.getActions, openAction: actionsAction.openAction }

TableActions = connect(mapState, mapDispatch)(TableActions)

export default TableActions
