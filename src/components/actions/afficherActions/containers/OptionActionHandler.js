import React, { Component } from 'react'
import OptionAction from '../presentationals/OptionAction'
import { connect } from 'react-redux'
import { actions } from 'redux/actions'
import { actions as actionsEtats } from 'redux/etats'

let timeoutID

// Todo : filtre etat et penser a d'autres filtres

class OptionActionHandler extends Component {
  constructor (props) {
    super(props)
    this.state = {
      nom: '',
      etat: '',
      datePasse: false,
      dateEcheance: {},
      datePrevue: {}
    }
  }

  componentWillMount () {
    this.props.getEtats()
  }

  handleOnChange = e => {
    clearTimeout(timeoutID)
    const { name, value } = e.target
    const change = { [name]: value }
    e.preventDefault()
    this.setState(change, () => this.getActions(600))
  }

  getActions = count => {
    timeoutID = setTimeout(() => {
      this.props.getActions({ ...this.state, page: 1 })
    }, count)
  }

  handleOnChangeWithoutDelay = change => {
    this.setState(change, () => this.getActions(0))
  }

  handleOnChangeDateEcheance = date => {
    this.setState({ dateEcheance: date }, () => this.getActions(0))
  }

  handleOnChangeDatePrevue = date => {
    this.setState({ datePrevue: date }, () => this.getActions(0))
  }

  render () {
    let { etats } = this.props
    let { nom, dateEcheance, datePrevue } = this.state
    return (
      <OptionAction
        nom={nom}
        etats={etats}
        dateEcheance={dateEcheance}
        datePrevue={datePrevue}
        handleOnChange={this.handleOnChange}
        handleOnChangeWithoutDelay={this.handleOnChangeWithoutDelay}
      />
    )
  }
}

const mapState = state => {
  return { etats: state.etats.etats }
}

const mapDispatch = { getActions: actions.getActions, getEtats: actionsEtats.getEtats }

OptionActionHandler = connect(mapState, mapDispatch)(OptionActionHandler)

export default OptionActionHandler
