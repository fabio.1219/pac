import React, { Component } from 'react'
import Field from 'components/global/Field'
import InputDatepicker from 'components/global/datepicker/InputDatepicker'

class OptionAction extends Component {
  handleOnChangeWithoutDelay = e => {
    const target = e.target
    const value = target.type === 'checkbox' ? target.checked : target.value
    const name = target.name
    const change = { [name]: value }
    this.props.handleOnChangeWithoutDelay(change)
  }

  handleOnChangeDateEcheance = date => {
    this.props.handleOnChangeWithoutDelay({ dateEcheance: date })
  }

  handleOnChangeDatePrevue = date => {
    this.props.handleOnChangeWithoutDelay({ datePrevue: date })
  }

  render () {
    const { nom, etats, etat, dateEcheance, datePrevue, datePassee, handleOnChange } = this.props
    return (
      <div className='card-content'>
        <div className='row'>
          <div className='column'>
            <Field label={'Nom du client'}>
              <div className='icon-input'>
                <input
                  type='text'
                  placeholder='Chercher un client'
                  name='nom'
                  value={nom}
                  onChange={handleOnChange}
                />
                <i className='fa fa-search' aria-hidden='true' />
              </div>
            </Field>
          </div>
          <div className='column'>
            <Field label={"Date d'échéance"}>
              <InputDatepicker
                selectedDate={dateEcheance}
                handleOnChange={this.handleOnChangeDateEcheance}
              />
            </Field>
          </div>
          <div className='column'>
            <Field label={'Date prévue'}>
              <InputDatepicker
                selectedDate={datePrevue}
                handleOnChange={this.handleOnChangeDatePrevue}
              />
            </Field>
          </div>
        </div>
        <div className='row'>
          <div className='column'>
            <Field label={'Etat'}>
              <div className='form-select'>
                <select
                  className='select'
                  name='etat'
                  value={etat}
                  onChange={this.handleOnChangeWithoutDelay}
                >
                  <option value=''>Tous les états</option>
                  {etats.map(etat => <option value={etat.id}>{etat.nom}</option>)}
                </select>
              </div>
            </Field>
          </div>
          <div className='column'>
            <div className='form-checkbox' style={{marginTop: '1.2rem'}}>
              <input
                type='checkbox'
                name='datePassee'
                onChange={this.handleOnChangeWithoutDelay}
                checked={datePassee}
              />
              Afficher les actions dont la date d'échéance est passée
            </div>
          </div>
          <div className='column' />
        </div>
      </div>
    )
  }
}

export default OptionAction
