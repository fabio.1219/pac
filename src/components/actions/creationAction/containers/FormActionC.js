import React, { Component } from 'react'
import FormAction from '../presentationals/FormAction'
import FormHandlerHOC from 'components/global/FormHandlerHOC'
import { Redirect } from 'react-router-dom'

import { actions } from 'redux/entreprendreaction'
import { connect } from 'react-redux'
import { actions as actionsAction } from 'redux/action'

class FormActionC extends Component {
  state = {
    typeActionR: '',
    typeActionNR: '',
    horsCategorie: false,
    commentaire: '',
    date: {},
    validation: {},
    redirect: false,
    idToRedirect: 0
  }

  handleOnChange = ({ change }) => {
    this.setState(change)
  }

  handleOnChangeDate = date => {
    this.setState({ date })
  }

  FormHandled = FormHandlerHOC(this.handleOnChange, 0)(FormAction)

  _typesActionRecommandes = () =>
    this.props.typesaction.filter(typesaction =>
      typesaction.categories.includes(this.props.client.categorie)
    )

  _typesActionNonRecommandes = () =>
    this.props.typesaction.filter(
      typesaction => !typesaction.categories.includes(this.props.client.categorie)
    )

  componentWillMount () {
    this.props.getTypesaction()
  }

  customValidation = () => {
    const { typeActionR, typeActionNR, horsCategorie, commentaire, date } = this.state
    let validation = {}
    let valid = true
    if (!date.formatDate) {
      validation.date = ["Une date d'échéance est obligatoire."]
      valid = false
    }
    if (!horsCategorie) {
      if (!typeActionR) {
        validation.typeActionR = ["Un type d'action doit être choisi."]
        valid = false
      }
    } else {
      if (!typeActionNR) {
        validation.typeActionNR = ["Un type d'action doit être choisi."]
        valid = false
      }
      if (commentaire.length === 0) {
        validation.commentaire = ['Une justification est nécessaire pour une action non-recommandée.']
        valid = false
      }
    }
    this.setState({ validation })
    return valid
  }

  handleOnClick = async () => {
    const state = this.state
    if (this.customValidation()) {
      let actionToCreate = {
        horsCategorie: state.horsCategorie,
        commentaire: state.commentaire,
        typeaction: state.horsCategorie ? state.typeActionNR : state.typeActionR,
        dateEcheance: state.date.formatDate,
        client: this.props.client.id
      }
      const { payload } = await this.props.createAction(actionToCreate)
      const action = payload.response.data
      this.props.openAction({action})
      this.setState({ redirect: true, idToRedirect: action.id })
    }
  }

  render () {
    const _typesActionRecommandes = this._typesActionRecommandes()
    const _typesActionNonRecommandes = this._typesActionNonRecommandes()
    const FormHandled = this.FormHandled
    const { redirect, validation,idToRedirect } = this.state
    if (!redirect) {
    return (
      <div>
        <FormHandled
          typesActionRecommandes={_typesActionRecommandes}
          typesActionNonRecommandes={_typesActionNonRecommandes}
          form={this.state}
          handleOnChange={this.handleOnChange}
          handleOnChangeDate={this.handleOnChangeDate}
          validation={validation}
        />
        <div className='all-separateur' />
        <div className='card-content'>
          <button className='button' onClick={this.handleOnClick}>Valider</button>
        </div>
      </div>
    )
    } else {
      return <Redirect to={`/actions/${idToRedirect}`} />
    }
  }
}

const mapStateToProps = state => {
  return {
    typesaction: state.entreprendreaction.typesaction,
    client: state.entreprendreaction.client
  }
}

const mapDispatchToProps = {
  getTypesaction: actions.getTypesaction,
  createAction: actions.createAction,
  openAction: actionsAction.openAction
}

FormActionC = connect(mapStateToProps, mapDispatchToProps)(FormActionC)

export default FormActionC
