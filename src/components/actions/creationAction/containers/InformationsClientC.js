import React, { Component } from 'react'
import InformationsClient from '../presentationals/InformationsClient'
import { connect } from 'react-redux'
import { actions } from 'redux/entreprendreaction'

class InformationsClientC extends Component {
  componentWillMount () {
    const { client, getClient, match } = this.props
    if (!client.id) {
      getClient(+match.params.id)
    }
  }

  render () {
    const { client } = this.props
    return (
      <div>
        <InformationsClient {...client} />
      </div>
    )
  }
}

const mapStateToProps = state => {
  return {
    client: state.entreprendreaction.client
  }
}

const mapDispatchToProps = { getClient: actions.getClient }

InformationsClientC = connect(mapStateToProps, mapDispatchToProps)(InformationsClientC)

export default InformationsClientC
