import React from 'react'
import Field from 'components/global/Field'
import Datepicker from 'components/global/datepicker/Datepicker'

const FormAction = ({
  typesActionRecommandes,
  typesActionNonRecommandes,
  typeActionR,
  typeActionNR,
  horsCategorie,
  date,
  commentaire,
  validation,
  handleOnChange,
  handleOnChangeDate
}) => {
  return (
    <div className='card-content'>
      <h3>Planification de l'action</h3>
      <div className='form row'>
        <div className='form-column'>
          <Field
            label={'Actions recommandées pour ce client'}
            isDisabled={horsCategorie}
            validation={validation['typeActionR']}
          >
           {typesActionRecommandes.length > 0 ?
            <div className='form-select'>
              <select name='typeActionR' value={typeActionR} onChange={handleOnChange} readOnly>
                <option value=''>Choisir une action...</option>
                {typesActionRecommandes.map(typesAction => (
                  <option key={typesAction.id} value={typesAction.id}>{typesAction.nom}</option>
                ))}
              </select>
            </div>
            :
            <p>Il y a aucun type d'action recommandée pour la catégorie de ce client.</p>
            }
          </Field>
          <div className='form-checkbox'>
            <input
              type='checkbox'
              name='horsCategorie'
              onChange={handleOnChange}
              checked={horsCategorie}
            />
            Choisir une action hors-catégorie pour ce client
          </div>
          <Field
            label={'Actions hors-catégorie'}
            isDisabled={!horsCategorie}
            validation={validation['typeActionNR']}
          >
            {typesActionNonRecommandes.length > 0 ?
              <div className='form-select'>
              <select name='typeActionNR' value={typeActionNR} onChange={handleOnChange}>
                <option value=''>Choisir une action...</option>
                {typesActionNonRecommandes.map(typesAction => (
                  <option key={typesAction.id} value={typesAction.id}>{typesAction.nom}</option>
                ))}
              </select>
            </div>
            :
            <p>Il y a aucun type d'action non recommandée pour la catégorie de ce client.</p>
            }
          </Field>
          <Field
            label={'Justification du choix'}
            isDisabled={!horsCategorie}
            validation={validation['commentaire']}
          >
            <textarea
              className='noresize'
              rows={3}
              name='commentaire'
              value={commentaire}
              onChange={handleOnChange}
            />
          </Field>
        </div>
        <div className='form-column'>
          <Field label={'A entreprendre avant le'} validation={validation['date']}>
            <Datepicker selectedDate={date} handleOnChange={handleOnChangeDate} />
          </Field>
        </div>
      </div>
    </div>
  )
}

export default FormAction
