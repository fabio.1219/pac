import React from 'react'
import Field from 'components/global/Field'

const InformationsClient = ({ nom, groupe, secteur, referant, categorie }) => {
  return (
    <div className='card-content'>
      <h3>Informations du client</h3>
      <div className='form row'>
        <div className='form-column'>
          <Field label={'Client'}>
            {nom}
          </Field>
          <Field label={'Groupe'}>
            {groupe}
          </Field>
          <Field label={'Secteur'}>
            {secteur}
          </Field>
          <Field label={'Notre référant'}>
            {referant && referant.nom}
          </Field>
          <Field label={'Catégorie du client'}>
            <div className={`badge-categorie badge-categorie--${categorie}`}>{categorie}</div>
          </Field>
        </div>
      </div>
    </div>
  )
}

export default InformationsClient
