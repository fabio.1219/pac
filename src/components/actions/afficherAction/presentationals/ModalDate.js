import React, { Component } from 'react'
import Modal from 'components/global/Modal'
import Datepicker from 'components/global/datepicker/Datepicker'

class ModalDate extends Component {
  state = {
    date: {},
    detail: this.props.detail || '',
    validation: ''
  }

  handleOnChange = e => {
    const { name, value } = e.target
    this.setState({ [name]: value })
  }

  handleOnChangeDate = date => {
    this.setState({ date })
  }

  handleOnClickValider = e => {
    if (this.state.date.formatDate) {
      this.setState({ validation: '' })
      const { updateAction, etatModal, id, etat, commentaire } = this.props
      const maj = {
        action: {
          [etatModal]: this.state.date.formatDate,
          detail: this.state.detail
        },
        etat: {
          etat,
          commentaire
        }
      }
      updateAction(id, maj)
    } else {
      this.setState({ validation: 'Une date est obligatoire.' })
    }
  }

  render () {
    const { date, detail, validation } = this.state
    const { changeModal, etatModal, titre } = this.props
    return (
      <Modal handleOnClickClose={changeModal}>
        <div className='card'>
          <h2 className='card-header'>{titre}</h2>
          <div className='card-content'>
            <Datepicker
              className='center'
              selectedDate={date}
              handleOnChange={this.handleOnChangeDate}
            />
            <div className='form-field--error'>{validation}</div>
            {etatModal === 'datePrevue' &&
              <div className='form-field mt1'>
                <label>Détails supplémentaires</label>
                <textarea
                  name='detail'
                  className='noresize'
                  rows={3}
                  value={detail}
                  onChange={this.handleOnChange}
                />
              </div>}
          </div>
          <div className='all-separateur' />
          <div className='card-content'>
            <button className='button' onClick={this.handleOnClickValider}>Valider</button>
            <button className='simple-button-icon simple-button-icon--red' onClick={changeModal}>
              Annuler
            </button>
          </div>
        </div>
      </Modal>
    )
  }
}

export default ModalDate
