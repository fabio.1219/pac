import React from 'react'
import Field from 'components/global/Field'

const InformationsAction = ({
  typeaction,
  detail,
  dateEcheance,
  datePrevue,
  showButtons,
  handleOnClickDateEcheance,
  handleOnClickDatePrevue
}) => {
  return (
    <div className='card-content'>
      <h3>Informations de l'action</h3>
      <div className='form row'>
        <div className='form-column'>
          <Field label={"Type d'action"}>
            {typeaction}
          </Field>
          <Field label={'Prévoir avant le'}>
            <div className='align-to-button--text'>
              {dateEcheance}
            </div>
            {showButtons &&
              !datePrevue &&
              <button
                className='simple-button-icon simple-button-icon--primary left-margin'
                onClick={handleOnClickDateEcheance}
              >
                Replanifier ?
              </button>}
          </Field>
          <Field label={"Date prévue pour l'action"}>
            <div className='align-to-button--text'>
              {datePrevue || 'Pas de date prévue'}
            </div>
            {showButtons &&
              <button
                className='simple-button-icon simple-button-icon--primary left-margin'
                onClick={handleOnClickDatePrevue}
              >
                {!datePrevue ? 'Planifier ?' : 'Replanifier ?'}
              </button>}
          </Field>
          {detail &&
            <Field label={"Détails de l'action"}>
              <div className='rl'>{detail}</div>
            </Field>}
        </div>
      </div>
    </div>
  )
}

export default InformationsAction
