import React from 'react'

const Etat = ({ etat, index }) => {
  const [date, heure] = etat.created_at.split(' ')
  const [an, mois, jour] = date.split('-')
  const formatDate = `${jour}.${mois}.${an}`
  return (
    <div className='row'>
      <div className='historique'>
        <div className='index'>
          {`${index + 1}.`}
        </div>
        <div className='content'>
          <div className='etat'>
            {`Etat "${etat.etat.nom}"`}
          </div>
          <div className='detail'>
            {`Mis à jour par ${etat.utilisateur.nom} le ${formatDate} à ${heure}`}
          </div>
          {etat.commentaire &&
            <div className='motif'>
              {`Motif : ${etat.commentaire}`}
            </div>}
        </div>
      </div>
    </div>
  )
}

const HistoriqueEtat = ({ etataction }) => {
  return (
    <div className='card-content'>
      <h3>Historique des états</h3>
      {etataction.map((etat, index) => <Etat etat={etat} index={index} key={index} />)}
    </div>
  )
}

export default HistoriqueEtat
