import React, { Component } from 'react'
import InformationsActionC from '../containers/InformationsActionC'
import InformationsClientC from '../containers/InformationsClientC'

class InfoGeneral extends Component {
  render () {
    return (
      <div className='row no-margin'>
        <div className='column _25'>
          <InformationsClientC />
        </div>
        <div className='separateur-vertical separateur-vertical--column' />
        <div className='column _75'>
          <InformationsActionC />
        </div>
      </div>
    )
  }
}

export default InfoGeneral
