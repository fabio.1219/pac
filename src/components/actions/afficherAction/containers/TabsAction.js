import TabsHOC from 'components/global/TabsHOC'
import InfoGeneral from '../presentationals/InfoGeneral'
import HistoriqueEtatC from '../containers/HistoriqueEtatC'
import React, { Component } from 'react'
import Loader from 'components/global/loader/Loader'
import { connect } from 'react-redux'

const TABS = [
  {
    nom: 'InfoGeneral',
    nomFormat: 'Informations principales',
    component: InfoGeneral
  },
  {
    nom: 'HistoriqueEtatC',
    nomFormat: 'Historique des états',
    component: HistoriqueEtatC
  }
]

class TabsAction extends Component {
  state = {
    etatTab: 'InfoGeneral'
  }

  setEtatTab = etatTab => {
    this.setState(etatTab)
  }

  Tabs = TabsHOC(TABS, this.setEtatTab)

  render () {
    if (this.props.isFetching) {
      return <Loader />
    }
    const Tabs = this.Tabs
    return <Tabs etat={this.state.etatTab} />
  }
}

const mapStateToProps = state => {
  return {
    isFetching: state.action.isFetching
  }
}

const mapDispatchToProps = null

TabsAction = connect(mapStateToProps, mapDispatchToProps)(TabsAction)

export default TabsAction
