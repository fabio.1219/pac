import React, { Component } from 'react'
import { connect } from 'react-redux'
import ModalDate from '../presentationals/ModalDate'
import { actions } from 'redux/action'

class ModalDateC extends Component {
  render () {
    const { id, showModal, etatModal, changeModal, updateAction, detail } = this.props
    if (showModal) {
      let args
      if (etatModal === 'datePrevue') {
        args = {
          etat: 3,
          titre: "Prévoir une date de l'action",
          commentaire: "Mise à jour de la date prévue de l'action"
        }
      } else {
        args = {
          etat: 2,
          titre: "Entreprendre l'action avant le",
          commentaire: "Mise à jour de la date d'échéance de l'action"
        }
      }
      const props = {
        updateAction,
        changeModal,
        etatModal,
        id,
        detail,
        etat: args.etat,
        titre: args.titre,
        commentaire: args.commentaire
      }
      return <ModalDate {...props} />
    } else {
      return null
    }
  }
}

const mapStateToProps = state => {
  return {
    id: state.action.id,
    showModal: state.action.showModal,
    etatModal: state.action.etatModal,
    detail: state.action.detail
  }
}

const mapDispatchToProps = { changeModal: actions.changeModal, updateAction: actions.updateAction }

ModalDateC = connect(mapStateToProps, mapDispatchToProps)(ModalDateC)

export default ModalDateC
