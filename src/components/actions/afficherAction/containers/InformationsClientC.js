import React, { Component } from 'react'
import InformationsClient from '../../creationAction/presentationals/InformationsClient'
import { connect } from 'react-redux'

class InformationsClientC extends Component {
  render () {
    const { client } = this.props
    return (
      <div>
        <InformationsClient {...client} />
      </div>
    )
  }
}

const mapStateToProps = state => {
  return {
    client: state.action.client
  }
}

const mapDispatchToProps = null

InformationsClientC = connect(mapStateToProps, mapDispatchToProps)(InformationsClientC)

export default InformationsClientC
