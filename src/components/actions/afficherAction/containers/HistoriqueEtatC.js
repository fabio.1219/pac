import { connect } from 'react-redux'
import HistoriqueEtat from '../presentationals/HistoriqueEtat'

const mapStateToProps = state => {
  return {
    etataction: state.action.etataction
  }
}

const mapDispatchToProps = {}

const HistoriqueEtatC = connect(mapStateToProps, mapDispatchToProps)(HistoriqueEtat)

export default HistoriqueEtatC
