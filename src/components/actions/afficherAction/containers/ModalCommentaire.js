import React, { Component } from 'react'
import Modal from 'components/global/Modal'
import { connect } from 'react-redux'
import { actions } from 'redux/action'

function ModalTexte (updateAction, changeModal, id, etat, titre) {
  return class ModalTexte extends Component {
    state = {
      commentaire: '',
      validation: ''
    }

    handleOnChange = e => {
      const { name, value } = e.target
      this.setState({ [name]: value })
    }

    handleOnClickValider = e => {
      const { commentaire } = this.state
      if (commentaire.length > 0) {
        const maj = {
          etat: {
            etat,
            commentaire: this.state.commentaire
          }
        }
        updateAction(id, maj)
      } else {
        this.setState({ validation: "Un commentaire est demandé pour la mise à jour de l'action." })
      }
    }

    render () {
      const { commentaire, validation } = this.state
      return (
        <Modal handleOnClickClose={changeModal}>
          <div className='card'>
            <h2 className='card-header'>{titre}</h2>
            <div className='card-content'>
              <textarea
                className='textarea-modal'
                name='commentaire'
                value={commentaire}
                onChange={this.handleOnChange}
              />
              <div className='form-field--error'>{validation}</div>
            </div>
            <div className='all-separateur' />
            <div className='card-content'>
              <button className='button' onClick={this.handleOnClickValider}>Valider</button>
              <button className='simple-button-icon simple-button-icon--red' onClick={changeModal}>Annuler</button>
            </div>
          </div>
        </Modal>
      )
    }
  }
}

class ModalCommentaire extends Component {
  render () {
    const { id, showModalFin, etatModalFin, changeModalFin, updateAction } = this.props
    if (showModalFin) {
      let args
      if (etatModalFin === 'cloturer') {
        args = {
          etat: 4,
          titre: 'Ecrivez un commentaire si vous le souhaitez.'
        }
      } else {
        args = {
          etat: 5,
          titre: 'Pour annuler une action, précisez une raison.'
        }
      }
      const ModalToDisplay = ModalTexte(updateAction, changeModalFin, id, args.etat, args.titre)
      return <ModalToDisplay />
    } else {
      return null
    }
  }
}

const mapStateToProps = state => {
  return {
    id: state.action.id,
    showModalFin: state.action.showModalFin,
    etatModalFin: state.action.etatModalFin
  }
}

const mapDispatchToProps = {
  changeModalFin: actions.changeModalFin,
  updateAction: actions.updateAction
}

ModalCommentaire = connect(mapStateToProps, mapDispatchToProps)(ModalCommentaire)

export default ModalCommentaire
