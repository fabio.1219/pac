import InformationsAction from '../presentationals/InformationsAction'
import React, { Component } from 'react'
import { connect } from 'react-redux'
import { actions } from 'redux/action'

class InformationsActionC extends Component {
  handleOnClickDatePrevue = () => {
    this.props.changeModal({ etatModal: 'datePrevue' })
  }

  handleOnClickDateEcheance = () => {
    this.props.changeModal({ etatModal: 'dateEcheance' })
  }

  render () {
    const { typeaction, dateEcheance, datePrevue, detail, etat } = this.props
    return (
      <div>
        <InformationsAction
          typeaction={typeaction}
          dateEcheance={dateEcheance}
          datePrevue={datePrevue}
          detail={detail}
          showButtons={etat > 1 && etat < 4}
          handleOnClickDateEcheance={this.handleOnClickDateEcheance}
          handleOnClickDatePrevue={this.handleOnClickDatePrevue}
        />
      </div>
    )
  }
}

const mapStateToProps = state => {
  return {
    typeaction: state.action.typeaction.nom,
    dateEcheance: state.action.dateEcheance,
    datePrevue: state.action.datePrevue,
    etat: state.action.etatActuel.etat.id,
    detail: state.action.detail
  }
}

const mapDispatchToProps = { changeModal: actions.changeModal }

InformationsActionC = connect(mapStateToProps, mapDispatchToProps)(InformationsActionC)

export default InformationsActionC
