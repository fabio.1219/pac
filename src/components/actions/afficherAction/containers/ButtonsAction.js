import React, { Component } from 'react'
import { connect } from 'react-redux'
import { actions } from 'redux/action'

class ButtonsAction extends Component {
  handleOnClickCloturer = e => {
    this.props.changeModalFin({ etatModalFin: 'cloturer' })
  }

  handleOnClickAnnuler = e => {
    this.props.changeModalFin({ etatModalFin: 'annuler' })
  }

  handleOnClickApprouver = e => {
    const { updateAction, id } = this.props
    const newEtat = {
      etat: {
        etat: 2 //Nouvelle état
      }
    }
    updateAction(id, newEtat)
  }

  render () {
    if (this.props.isFetching) {
      return null
    }

    if (this.props.etat === 1) {
      return (
        <div className='card-content'>
          <button className='button' onClick={this.handleOnClickApprouver}>Approuver l'action</button>
          <button
            className='simple-button-icon simple-button-icon--red'
            onClick={this.handleOnClickAnnuler}
          >
            Annuler l'action
          </button>
        </div>
      )
    }

    if (this.props.etat < 4) {
      return (
        <div className='card-content'>
          <button className='button' onClick={this.handleOnClickCloturer}>Clôturer l'action</button>
          <button
            className='simple-button-icon simple-button-icon--red'
            onClick={this.handleOnClickAnnuler}
          >
            Annuler l'action
          </button>
        </div>
      )
    }

    return null
  }
}

const mapStateToProps = state => {
  return {
    id: state.action.id,
    etat: state.action.etatActuel.etat.id,
    isFetching: state.action.isFetching
  }
}

const mapDispatchToProps = { changeModalFin: actions.changeModalFin, updateAction: actions.updateAction }

ButtonsAction = connect(mapStateToProps, mapDispatchToProps)(ButtonsAction)

export default ButtonsAction
