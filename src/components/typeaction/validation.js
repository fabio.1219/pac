const validationTypeAction = {
  attributeNames: {
    nom: "Le nom du type d'action",
    categories: 'Une catégorie'
  },
  messagesErreur: {
    required: ':attribute est obligatoire.',
    'required.categories': "Il faut attribuer au moins une catégorie à un type d'action."
  },
  rules: {
    nom: 'required',
    categories: 'required'
  }
}

export default validationTypeAction
