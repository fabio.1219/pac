import React, { Component } from 'react'
import FormHandlerHOC from 'components/global/FormHandlerHOC'
import FormTypeAction from '../presentationals/FormTypeAction'
import ModalSupprimer from 'components/global/ModalSupprimer'

import valider from 'lib/Validation'
import validationTypeAction from '../validation'

import { connect } from 'react-redux'
import { actions } from '../../../redux/typesaction'

class FormTypeActionHandler extends Component {
  state = {
    validation: {},
    showModal: false
  }

  FormHandled = FormHandlerHOC(this.props.changeForm)(FormTypeAction)

  handleOnClickCategorie = e => {
    if (this.props.editMode) {
      let categories = this.props.form.categories.slice()
      const categoriePressed = +e.target.value
      if (categories.includes(categoriePressed)) {
        categories = categories.filter(categorie => categorie !== categoriePressed)
      } else {
        categories.push(categoriePressed)
      }
      this.props.changeForm({ change: { categories } })
    }
  }

  getButtons = () => {
    if (this.props.addMode) {
      return <button className='button' onClick={this.handleOnClickEnregistrer}>Enregistrer</button>
    }

    if (this.props.editMode) {
      return (
        <div>
          <button className='button' onClick={this.handleOnClickEnregistrer}>Enregistrer</button>
          <button
            className='simple-button-icon simple-button-icon--red'
            onClick={this.handleOnClickAnnuler}
          >
            Annuler
          </button>
        </div>
      )
    } else {
      return (
        <div>
          <button className='button' onClick={this.handleOnClickModifier}>Modifier</button>
          <button
            className='simple-button-icon simple-button-icon--red'
            onClick={this.handleShowModal}
          >
            Supprimer
          </button>
        </div>
      )
    }
  }

  handleShowModal = e => {
    this.setState({ showModal: true })
  }
  handleHideModal = e => {
    this.setState({ showModal: false })
  }

  handleOnClickSupprimer = async e => {
    e.preventDefault()
    const { form, delTypeAction, getTypesAction } = this.props
    await delTypeAction(form.id)
    getTypesAction({ paginate: true })
  }

  handleOnClickModifier = () => {
    this.props.switchEditMode()
  }

  handleOnClickAnnuler = () => {
    this.props.cancelChangeForm()
  }

  handleOnClickEnregistrer = async () => {
    const { form, addMode, updateTypeAction, getTypesAction, addTypeAction } = this.props
    const resultat = valider(form, validationTypeAction)
    if (resultat.reussite) {
      this.setState({ validation: {} })
      if (addMode) {
        await addTypeAction(form)
        await getTypesAction({ paginate: true })
      } else {
        updateTypeAction(form.id, form)
      }
    } else {
      this.setState({ validation: resultat.validation })
    }
  }

  handleOnClickClose = () => {
    this.props.closeForm()
  }

  render () {
    const { form, showForm, editMode } = this.props
    const FormHandled = this.FormHandled
    if (showForm) {
      return (
        <div>
          <div className='card-content'>
            <FormHandled
              form={form}
              modeSelection
              selected={form.categories}
              isDisabled={!editMode}
              handleOnClickClose={this.handleOnClickClose}
              handleOnClickCategorie={this.handleOnClickCategorie}
              validation={this.state.validation}
            />
          </div>
          <div className='all-separateur' />
          <div className='card-content'>
            {this.getButtons()}
          </div>
          {this.state.showModal &&
            <ModalSupprimer
              handleOnClickAnnuler={this.handleHideModal}
              handleOnClickSupprimer={this.handleOnClickSupprimer}
            />}
        </div>
      )
    } else {
      return null
    }
  }
}

const mapStateToProps = state => {
  return {
    form: state.typesaction.form,
    showForm: state.typesaction.showForm,
    editMode: state.typesaction.editMode,
    addMode: state.typesaction.addMode
  }
}

const mapDispatchToProps = {
  changeForm: actions.changeForm,
  switchEditMode: actions.switchEditMode,
  cancelChangeForm: actions.cancelChangeForm,
  updateTypeAction: actions.updateTypeAction,
  getTypesAction: actions.getTypesAction,
  closeForm: actions.closeForm,
  addTypeAction: actions.addTypeAction,
  delTypeAction: actions.delTypeAction
}

FormTypeActionHandler = connect(mapStateToProps, mapDispatchToProps)(FormTypeActionHandler)

export default FormTypeActionHandler
