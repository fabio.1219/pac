import React, { Component } from 'react'
import { connect } from 'react-redux'
import Table from '../../global/Table'
import Pagination from '../../global/pagination'
import { actions } from '../../../redux/typesaction'
import Loader from 'components/global/loader/Loader'

const ENTETES = ['Nom', 'Catégories concernées']
const KEYS = [
  typeaction => typeaction.nom, 
  typeaction => (
    <div className="margin-badge--row">
    {typeaction.categories.map(categorie => (
      <div className="margin-badge--column" key={categorie}>
        <div className={`badge-categorie badge-categorie--${categorie}`}>{categorie}</div>
      </div>
    ))}
    </div>
  )
]

class TableTypeAction extends Component {
  handleOnClick = e => {
    e.preventDefault()
    this.props.getTypesAction({ paginate: true, page: e.target.name[e.target.name.length - 1] })
  }

  componentWillMount () {
    this.props.getTypesAction({ paginate: true })
  }

  handleOpen = (e, typeaction, id) => {
    this.props.open({ typeaction })
  }

  handleNew = () => {
    this.props.newForm()
  }

  render () {
    let {
      isFetching,
      from,
      to,
      total,
      current_page,
      last_page,
      prev_page_url,
      next_page_url,
      data
    } = this.props.typesaction
    return (
      <div>
        <div className='card-content'>
          <button className='button' onClick={this.handleNew}>Ajouter un type d'action</button>
        </div>
        <div className='all-separateur' />
        {!isFetching && data.length > 0
          ? <div className='card-content'>
            <div className='table-info'>
              <div className='table-info-no'>{from} - {to}</div>
              <div className='table-info-text'> types d'action sur </div>
              <div className='table-info-no'>{total}</div>
            </div>
            <Table entetes={ENTETES} rows={data} keys={KEYS} handleOpen={this.handleOpen} />
            <Pagination
              prev_page_url={prev_page_url}
              next_page_url={next_page_url}
              current_page={current_page}
              last_page={last_page}
              handleOnClick={this.handleOnClick}
              />
          </div>
          : isFetching
              ? <Loader loading={true} />
              : <div className='card-content notfound-p'><p>Aucun type d'action trouvé.</p></div>}
      </div>
    )
  }
}

const mapState = state => {
  return {
    typesaction: state.typesaction
  }
}

const mapDispatch = {
  getTypesAction: actions.getTypesAction,
  open: actions.open,
  newForm: actions.newForm
}

TableTypeAction = connect(mapState, mapDispatch)(TableTypeAction)

export default TableTypeAction
