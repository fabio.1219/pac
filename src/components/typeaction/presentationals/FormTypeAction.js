import React from 'react'
import Field from 'components/global/Field'
import Text from 'components/global/Text'
import Categories from 'components/global/Categories'
import TitleAndButton from 'components/global/TitleAndButton'

const LABELS = ['Sans catégorie', '1', '2', '3', '4', '5', '6', '7', '8', '9']

const FormTypeAction = ({
  validation,
  nom,
  handleOnChange,
  isDisabled,
  modeSelection,
  selected,
  handleOnClickClose,
  handleOnClickCategorie
}) => {
  return (
    <div>
      <TitleAndButton title={"Information du type d'action"}>
        <button
          className='align-to-button--text simple-button-icon simple-button-icon--table simple-button-icon--red'
          onClick={handleOnClickClose}
        >
          <i className='fa fa fa-times' aria-hidden='true' />
        </button>
      </TitleAndButton>
      <div className='form row'>
        <div className='form-column'>
          <Field label={'Nom'} validation={validation['nom']}>
            {isDisabled
              ? <Text>{nom}</Text>
              : <input type='text' name='nom' value={nom} onChange={handleOnChange} />}
          </Field>
          <Field label={'Catégories visées'} validation={validation['categories']}>
            <Categories
              labels={LABELS}
              modeSelection={modeSelection}
              isDisabled={isDisabled}
              showSansCategorie={false}
              selected={selected}
              handleOnClick={handleOnClickCategorie}
            />
          </Field>
        </div>
      </div>
    </div>
  )
}

export default FormTypeAction
