import React from 'react'
import GraphiqueMain from '../containers/GraphiqueMain'
import NombresC from '../containers/NombresC'
import ButtonSave from '../containers/ButtonSave'
import ButtonDelete from '../containers/ButtonDelete'
import { connect } from 'react-redux'
import { formateDateComptoDate } from 'lib/DateFormat'

const AfficherStat = ({ date }) => {
  return (
    <div className='card-content'>
      <div className='container-2-elements'>
        <h3 className='container-2-elements--first align-to-button--title'>
          {'Statistiques ' + (date ? `du ${formateDateComptoDate(date)}` : 'actuelles')}
        </h3>
        {!date ? <ButtonSave /> : <ButtonDelete />}
      </div>
      <div className='row no-margin marg1'>
        <div className='column _35'>
          <NombresC />
        </div>
        <div className='column _65'>
          <GraphiqueMain />
        </div>
      </div>
    </div>
  )
}

const mapStateToProps = state => {
  return {
    date: state.statistiques.select.created_at
  }
}

const mapDispatchToProps = null

export default connect(mapStateToProps, mapDispatchToProps)(AfficherStat)
