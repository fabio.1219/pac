import React, { Component } from 'react'
import hoverblur from '../hoverblur'

class Nombre extends Component {
  onMouseOver = () => {
    const { events } = this.props
    events && events.onMouseOver()
  }

  onMouseOut = () => {
    const { events } = this.props
    events && events.onMouseOut()
  }

  render () {
    const { titre, nb, svg } = this.props
    return (
      <div className='nombre' onMouseOver={this.onMouseOver} onMouseOut={this.onMouseOut}>
        <div className='titre'>
          {titre}
        </div>
        <div className='chiffre'>
          {nb}
          {svg &&
            <svg className='svg'>
              <circle className='dot' cx='50%' cy='50%' r={svg.r} fill={svg.color} />
            </svg>}
        </div>
      </div>
    )
  }
}

const Nombres = ({ nbClients, sansCategories, groupes, arrayColor }) => {
  return (
    <div>
      <div className='section-nombres'>
        <h4 className='titre-nombres'>En général</h4>
        <div className='row'>
          <div className='column-nombres'>
            <Nombre titre='Clients au total' nb={nbClients} />
          </div>
          <div className='column-nombres'>
            <Nombre titre='Clients sans catégorie' nb={sansCategories} />
          </div>
        </div>
      </div>
      <div className='section-nombres'>
        <h4 className='titre-nombres'>Clients par groupes</h4>
        <div className='row'>
          {groupes.map((groupe, index) => (
            <div className='column-nombres' key={index}>
              <Nombre
                titre={groupe.nom}
                nb={groupe.total}
                events={hoverblur[index]}
                svg={arrayColor[index]}
              />
            </div>
          ))}
        </div>
      </div>
    </div>
  )
}

export default Nombres
