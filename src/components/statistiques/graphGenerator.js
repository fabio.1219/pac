import { axisBottom, axisLeft } from 'd3-axis'
import { scaleLinear } from 'd3-scale'
import { select } from 'd3-selection'
import { interpolateHcl } from 'd3-interpolate'
import { rgb } from 'd3-color'
import hoverblur from './hoverblur'

const labelX = ['Faible', 'Moyen', 'Fort']
const labelY = ['Basse', 'Moyenne', 'Elevée']

let g

const graphGenerator = (node, total, data, updateArrayColor) => {
  /**
   * =================================
   * 1. définir les longueurs et marge du graphique
   * =================================
   */
  const margin = {top: 33, right: 33, bottom: 50, left: 90}
  const width = 400 - margin.left - margin.right
  const height = 400 - margin.top - margin.bottom

  const xScale = scaleLinear().domain([1, 3]).range([0, width]).nice()

  const yScale = scaleLinear().domain([1, 3]).range([height, 0]).nice()

  if (g) {
    g.selectAll("*").remove()
  }

    /**
   * =================================
   * 2. Selectionner la balise svg et la modifier
   * =================================
   */
    const svg = select(node)
      .attr('width', width + margin.left + margin.right)
      .attr('height', height + margin.top + margin.bottom)

    g = svg.append('g').attr('transform', 'translate(' + margin.left + ',' + margin.top + ')')

    /**
   * =================================
   * 3. Définir les axes
   * =================================
   */
    // Scale = echelle, va permettre de placer les points
    // const xScale = scaleLinear().domain([1, 3]).range([0, width]).nice()

    // const yScale = scaleLinear().domain([1, 3]).range([height, 0]).nice()

    // Permets de définir la lignes des axes
    const xAxis = axisBottom(xScale)
      .ticks(2) // nb marque dans l'axe (essayer pour trouver le bon)
      .tickFormat((d, i) => labelX[i]) // donner un label texte au ticks

    const yAxis = axisLeft(yScale).ticks(2).tickFormat((d, i) => labelY[i])

    // Ajouter les axes au svg
    g
      .append('g')
      .attr('transform', 'translate(0,' + height + ')') // mettre l'axe en bas
      .attr("class", "axis")
      .call(xAxis)

    g.append("text")             
      .attr("transform",
            "translate(" + (width/2) + " ," + 
                           (height + margin.top + 10) + ")")
      .attr("class", "axis-label")
      .style("text-anchor", "middle")
      .text("Potentiel")

    g.append('g')
    .attr("class", "axis").call(yAxis)

    g.append("text")
      .attr("transform", "rotate(-90)")
      .attr("y", 0 - margin.left)
      .attr("x",0 - (height / 2))
      .attr("dy", "1em")
      .attr("class", "axis-label")
      .style("text-anchor", "middle")
      .text("Connaissance")

  /**
   * =================================
   * 4. Définir les points
   * =================================
   */


  const circlesGroup = g.append('g')

  // On prend tous les points existants et on supprime tout
  circlesGroup.selectAll('circle').remove()

  // on ajoute les données en tant que cercle
  const circles = circlesGroup.selectAll('circle').data(data)

  // On génère des couleurs pour les attribuer à chaque point
  const color = scaleLinear()
    .domain([1, data.length])
    .interpolate(interpolateHcl)
    .range([rgb('#007AFF'), rgb('#FFF500')])

  
  // On prend les données et on les traites
  circles
    .enter()
    .append('circle')
    .attr('class', 'dot')
    .attr('id', function (d) {
      return d.nom
    })
    .attr('r', function (d) {
      return height / 100 + (d.total * (1 / total) * 15)
    })
    .attr('cx', function (d) {
      return xScale(d.connaissance)
    })
    .attr('cy', function (d) {
      return yScale(d.potentiel)
    })
    .style('fill', function (d, index) {
      return color(index)
    })

  const circlesNodes = circlesGroup.selectAll('circle').nodes()

  const arrayColor = data.map((value, index) => {
    hoverblur[index] = {
      onMouseOver: () => {
        
        circlesGroup.append(function() { return circlesNodes[index] })
        select(circlesNodes[index]).attr('r', function (d) {
          return (height / 100 + (d.total * (1 / total) * 15)) * 1.8
        }).style('z-index', '1')
      },
      onMouseOut: () => {
        select(circlesNodes[index]).attr('r', function (d) {
          return height / 100 + (d.total * (1 / total) * 15)
        }).style('z-index', '0')
      }
    }
    return {
      nom: value.nom,
      r: height / 100 + (value.total * (1 / total) * 15),
      color: color(index)
    }
  })

  updateArrayColor({ arrayColor })
}

export default graphGenerator
