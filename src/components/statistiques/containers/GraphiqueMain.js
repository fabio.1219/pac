import React, { Component } from 'react'
import graphGenerator from '../graphGenerator'
import { connect } from 'react-redux'
import { actions } from 'redux/statistiques'

class GraphiqueMain extends Component {
  componentDidMount () {
    const { nbClients, groupes, updateArrayColor } = this.props
    graphGenerator(this.node, nbClients, groupes, updateArrayColor)
  }

  componentDidUpdate () {
    const { nbClients, groupes, updateArrayColor } = this.props
    graphGenerator(this.node, nbClients, groupes, updateArrayColor)
  }

  render () {
    return <svg ref={node => (this.node = node)} />
  }
}

const mapStateToProps = state => {
  return {
    groupes: state.statistiques.select.groupes,
    nbClients: state.statistiques.select.nbClients - state.statistiques.select.sansCategories
  }
}

const mapDispatchToProps = {
  updateArrayColor: actions.updateArrayColor
}

GraphiqueMain = connect(mapStateToProps, mapDispatchToProps)(GraphiqueMain)

export default GraphiqueMain
