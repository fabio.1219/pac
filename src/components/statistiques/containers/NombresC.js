import React, { Component } from 'react'
import Nombres from '../presentationals/Nombres'
import { connect } from 'react-redux'

class NombresC extends Component {
  render () {
    const { groupes = [], nbClients, sansCategories, arrayColor } = this.props
    return (
      <div>
        <Nombres
          nbClients={nbClients}
          sansCategories={sansCategories}
          groupes={groupes}
          arrayColor={arrayColor}
        />
      </div>
    )
  }
}

const mapStateToProps = state => {
  return {
    groupes: state.statistiques.select.groupes,
    nbClients: state.statistiques.select.nbClients,
    sansCategories: state.statistiques.select.sansCategories,
    arrayColor: state.statistiques.arrayColor
  }
}

const mapDispatchToProps = {}

NombresC = connect(mapStateToProps, mapDispatchToProps)(NombresC)

export default NombresC
