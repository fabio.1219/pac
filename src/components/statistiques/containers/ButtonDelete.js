import React, { Component } from 'react'
import { connect } from 'react-redux'
import { actions } from 'redux/statistiques'

import ModalSupprimer from 'components/global/ModalSupprimer'

class ButtonSave extends Component {
  state = {
    showModal: false
  }

  handleShowModal = e => {
    this.setState({ showModal: true })
  }
  handleHideModal = e => {
    this.setState({ showModal: false })
  }

  handleOnClickSupprimer = async e => {
    e.preventDefault()
    const { id, delStatistiquesSaved, getStatistiquesSaved } = this.props
    await delStatistiquesSaved(id)
    getStatistiquesSaved()
  }

  render () {
    return (
      <div className='container-2-elements--second'>
        <button
          className='simple-button-icon simple-button-icon--red'
          onClick={this.handleShowModal}
        >
          {'Supprimer'}
        </button>
        {this.state.showModal &&
          <ModalSupprimer
            handleOnClickAnnuler={this.handleHideModal}
            handleOnClickSupprimer={this.handleOnClickSupprimer}
          />}
      </div>
    )
  }
}

const mapStateToProps = state => {
  return {
    id: state.statistiques.select.id
  }
}

const mapDispatchToProps = {
  delStatistiquesSaved: actions.delStatistiquesSaved,
  getStatistiquesSaved: actions.getStatistiquesSaved
}

ButtonSave = connect(mapStateToProps, mapDispatchToProps)(ButtonSave)

export default ButtonSave
