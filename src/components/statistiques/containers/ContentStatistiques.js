import React, { Component } from 'react'
import Loader from 'components/global/loader/Loader'
import AfficherStat from '../presentationals/AfficherStat'
import TableStatistiques from './TableStatistiques'
import { actions } from 'redux/statistiques'
import { connect } from 'react-redux'

class ContentStatistiques extends Component {
  componentWillMount () {
    this.props.getStatistiqueClients()
    this.props.getStatistiquesSaved()
  }

  render () {
    if (this.props.isFetching) {
      return <Loader />
    }
    return (
      <div className='row no-margin'>
        <div className='column _25'>
          <TableStatistiques />
        </div>

        <div className='separateur-vertical separateur-vertical--column' />
        <div className='column _75'>
          <AfficherStat />
        </div>
      </div>
    )
  }
}

const mapStateToProps = state => {
  return {
    isFetching: state.statistiques.isFetchingCurrent || state.statistiques.isFetchingSaved
  }
}

const mapDispatchToProps = {
  getStatistiqueClients: actions.getStatistiqueClients,
  getStatistiquesSaved: actions.getStatistiquesSaved
}

export default connect(mapStateToProps, mapDispatchToProps)(ContentStatistiques)
