import React, { Component } from 'react'
import { connect } from 'react-redux'
import Table from '../../global/Table'
import Pagination from '../../global/pagination'

import { formateDateComptoDate } from 'lib/DateFormat'
import { actions } from 'redux/statistiques'
import Loader from 'components/global/loader/Loader'

const ENTETES = ['Date']
const KEYS = [statistiques => (statistiques.created_at && formateDateComptoDate(statistiques.created_at)) || 'Maintenant']

class TableStatistiques extends Component {
  handleOnClick = e => {
    e.preventDefault()
    this.props.getStatistiquesSaved({ page: e.target.name[e.target.name.length - 1] })
  }

  handleOpen = (e, statistiques, id) => {
    this.props.open({ statistiques, isSelected: true })
  }

  statsSaved = () => {
    const {
      from,
      to,
      total,
      current_page,
      last_page,
      prev_page_url,
      next_page_url,
      data = []
    } = this.props.statsSaved
    const newData = [this.props.actuel, ...data]
    const newFrom = (from || 1)
    const newTo = (to || 0) + 1
    const newTotal = total + 1
    return {
      from: newFrom,
      to: newTo,
      total: newTotal,
      current_page,
      last_page,
      prev_page_url,
      next_page_url,
      data: newData
    }
  }

  render () {
    const {
      from,
      to,
      total,
      current_page,
      last_page,
      prev_page_url,
      next_page_url,
      data
    } = this.statsSaved()
    return (
      <div>
        {!this.props.isFetchingSaved && data.length > 0
          ? <div className='card-content'>
            <div className='table-info'>
              <div className='table-info-no'>{from} - {to}</div>
              <div className='table-info-text'> statistiques sur </div>
              <div className='table-info-no'>{total}</div>
            </div>
            <Table entetes={ENTETES} rows={data} keys={KEYS} handleOpen={this.handleOpen} />
            <Pagination
              prev_page_url={prev_page_url}
              next_page_url={next_page_url}
              current_page={current_page}
              last_page={last_page}
              handleOnClick={this.handleOnClick}
              />
          </div>
          : this.props.isFetchingSaved
              ? <Loader />
              : <div className='card-content'>Pas de statistiques</div>}
      </div>
    )
  }
}

const mapState = state => {
  return {
    isFetchingSaved: state.statistiques.isFetchingSaved,
    statsSaved: state.statistiques.statsSaved,
    actuel: {
      id: state.statistiques.actuel.id,
      nbClients: state.statistiques.actuel.nbClients,
      sansCategories: state.statistiques.actuel.sansCategories,
      groupes: state.statistiques.actuel.groupes
    }
  }
}

const mapDispatch = {
  open: actions.open,
  getStatistiquesSaved: actions.getStatistiquesSaved
}

TableStatistiques = connect(mapState, mapDispatch)(TableStatistiques)

export default TableStatistiques
