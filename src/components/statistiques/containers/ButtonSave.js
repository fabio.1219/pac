import React, { Component } from 'react'
import { connect } from 'react-redux'
import { actions } from 'redux/statistiques'

class ButtonSave extends Component {
  handleNew = async e => {
    const { postStatistiques, actuel, getStatistiquesSaved } = this.props
    await postStatistiques(actuel)
    await getStatistiquesSaved()
  }

  render () {
    return (
      <button className='button container-2-elements--second' onClick={this.handleNew}>
        {'Enregistrer'}
      </button>
    )
  }
}

const mapStateToProps = state => {
  return {
    actuel: {
      nbClients: state.statistiques.actuel.nbClients,
      sansCategories: state.statistiques.actuel.sansCategories,
      groupes: state.statistiques.actuel.groupes
    }
  }
}

const mapDispatchToProps = {
  postStatistiques: actions.postStatistiques,
  getStatistiquesSaved: actions.getStatistiquesSaved
}

ButtonSave = connect(mapStateToProps, mapDispatchToProps)(ButtonSave)

export default ButtonSave
