import React, { Component } from 'react'
import TableCategories from '../categories/containers/TableCategories'
import ChoixCategories from '../categories/containers/ChoixCategories'

class ClientsCategories extends Component {
  render () {
    return (
      <div className='card'>
        <h2 className='card-header'>Catégories</h2>
        <div className='row no-margin'>
          <div className='column _35'>
            <div className='card-content'>
              <ChoixCategories />
            </div>
          </div>
          <div className='separateur-vertical separateur-vertical--column' />
          <div className='column _65'>
            <div className='card-content'>
              <TableCategories history={this.props.history} />
            </div>
          </div>
        </div>
      </div>
    )
  }
}

export default ClientsCategories
