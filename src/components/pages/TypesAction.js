import React from 'react'
import TableTypeAction from '../typeaction/containers/TableTypeAction'
import FormTypeActionHandler from '../typeaction/containers/FormTypeActionHandler'
import { connect } from 'react-redux'

const TypesAction = ({ showForm }) => {
  return (
    <div className='card'>
      <h2 className='card-header'>Types d'action</h2>
      <div className='row no-margin'>
        <div className='column _55'>
          <TableTypeAction />
        </div>
        {showForm && <div className='separateur-vertical separateur-vertical--column' />}
        {showForm &&
          <div className='column _45'>
            <FormTypeActionHandler />
          </div>}
      </div>
    </div>
  )
}

const mapStateToProps = state => {
  return {
    showForm: state.typesaction.showForm
  }
}

const mapDispatchToProps = null

export default connect(mapStateToProps, mapDispatchToProps)(TypesAction)
