import React from 'react'
import { connect } from 'react-redux'

const Accueil = ({ nom }) => {
  return (
    <div className='card card-accueil'>
      <p>
        <div className='text'>
          Bienvenue<br />{`${nom} !`}
        </div>
      </p>
    </div>
  )
}

const mapStateToProps = state => {
  return {
    nom: state.login.decoded.nom
  }
}

const mapDispatchToProps = null

export default connect(mapStateToProps, mapDispatchToProps)(Accueil)
