import React, { Component } from 'react'
import TabsAction from '../actions/afficherAction/containers/TabsAction'
import ButtonsAction from '../actions/afficherAction/containers/ButtonsAction'
import ModalDateC from '../actions/afficherAction/containers/ModalDateC'
import ModalCommentaire from '../actions/afficherAction/containers/ModalCommentaire'
import { connect } from 'react-redux'
import { actions } from 'redux/action'

class AfficherAction extends Component {
  componentWillMount () {
    const { actionLoaded, getAction, match } = this.props
    if (!actionLoaded) {
      getAction(match.params.id)
    }
  }

  render () {
    return (
      <div>
        <div className='card'>
          <h2 className='card-header'>Entreprendre une action</h2>
          <TabsAction />
          <div className='all-separateur' />
          <ButtonsAction />
        </div>
        <ModalDateC />
        <ModalCommentaire />
      </div>
    )
  }
}

const mapStateToProps = state => {
  return {
    actionLoaded: state.action.actionLoaded
  }
}

const mapDispatchToProps = { getAction: actions.getAction }

AfficherAction = connect(mapStateToProps, mapDispatchToProps)(AfficherAction)

export default AfficherAction
