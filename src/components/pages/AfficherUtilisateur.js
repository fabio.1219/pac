import React, { Component } from 'react'
import FormUtilisateurUpdate from '../utilisateurs/containers/FormUtilisateurUpdate'
import { connect } from 'react-redux'
import { actions } from 'redux/utilisateurs'

class AfficherUtilisateur extends Component {
  componentWillMount () {
    this.props.getUtilisateur(this.props.match.params.id)
  }

  render () {
    return (
      <div className='card'>
        <h2 className='card-header'>Utilisateur</h2>
        <FormUtilisateurUpdate />
      </div>
    )
  }
}

const mapDispatch = { getUtilisateur: actions.getUtilisateur }

AfficherUtilisateur = connect(null, mapDispatch)(AfficherUtilisateur)

export default AfficherUtilisateur
