import React, { Component } from 'react'
import ContentStatistiques from '../statistiques/containers/ContentStatistiques'

class Statistiques extends Component {
  render () {
    return (
      <div className='card'>
        <h2 className='card-header'>Statistiques</h2>
        <ContentStatistiques />
      </div>
    )
  }
}

export default Statistiques
