import React, { Component } from 'react'
import OptionUtilisateurHandler from '../utilisateurs/containers/OptionUtilisateurHandler'
import TableUtilisateurs from '../utilisateurs/containers/TableUtilisateurs'

class Utilisateurs extends Component {
  render () {
    return (
      <div className='card'>
        <h2 className='card-header'>Utilisateurs</h2>
        <OptionUtilisateurHandler />
        <div className='all-separateur' />
        <div className='card-content'>
          <TableUtilisateurs history={this.props.history} />
        </div>
      </div>
    )
  }
}

export default Utilisateurs
