import React, { Component } from 'react'
import TabsClient from '../client/containers/afficherClient/TabsClient'
import { connect } from 'react-redux'
import { actions } from 'redux/client'

class AfficherClient extends Component {
  componentWillMount () {
    this.props.getClient(this.props.match.params.id)
  }

  render () {
    return (
      <div className='card'>
        <h2 className='card-header'>Client</h2>
        <TabsClient history={this.props.history} />
      </div>
    )
  }
}

const mapDispatch = { getClient: actions.getClient }

AfficherClient = connect(null, mapDispatch)(AfficherClient)

export default AfficherClient
