import React, { Component } from 'react';
import Steps from '../client/containers/ajouterClient/Steps'

class AjouterClient extends Component {
  render() {
    return (
      <div className="card">
        <h2 className="card-header">Ajouter un client</h2>
        <Steps history={this.props.history} />
      </div>
    );
  }
}

export default AjouterClient;