import React, { Component } from 'react';
import OptionClientHandler from '../clients/containers/OptionClientHandler'
import TableClients from '../clients/containers/TableClients'

class Clients extends Component {
  render() {
    return (
      <div className="card">
        <h2 className="card-header">Tous les clients</h2>
        <OptionClientHandler />
        <div className="all-separateur" />
        <TableClients history={this.props.history}/>
      </div>
    );
  }
}

export default Clients;