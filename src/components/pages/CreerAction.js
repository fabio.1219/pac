import React from 'react'
import InformationsClientC from '../actions/creationAction/containers/InformationsClientC'
import FormActionC from '../actions/creationAction/containers/FormActionC'

const CreerAction = ({ match }) => {
  return (
    <div className='card'>
      <h2 className='card-header'>Entreprendre une action</h2>
      <div className='row no-margin'>
        <div className='column _25'>
          <InformationsClientC match={match} />
        </div>
        <div className='separateur-vertical separateur-vertical--column' />
        <div className='column _75'>
          <FormActionC />
        </div>
      </div>
    </div>
  )
}

export default CreerAction
