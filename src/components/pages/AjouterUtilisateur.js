import React, { Component } from 'react'
import FormUtilisateurAjouter from '../utilisateurs/containers/FormUtilisateurAjouter'

class AjouterUtilisateur extends Component {
  render () {
    return (
      <div className='card'>
        <h2 className='card-header'>Ajouter un utilisateur</h2>
        <FormUtilisateurAjouter history={this.props.history} />
      </div>
    )
  }
}

export default AjouterUtilisateur
