import React, { Component } from 'react';
import OptionActionHandler from '../actions/afficherActions/containers/OptionActionHandler'
import TableActions from '../actions/afficherActions/containers/TableActions'

class AfficherActions extends Component {
  render() {
    return (
      <div className="card">
        <h2 className="card-header">Toutes les actions</h2>
        <OptionActionHandler />
        <div className="all-separateur" />
        <TableActions history={this.props.history}/>
      </div>
    );
  }
}

export default AfficherActions;