import React from 'react'

const FormChange = ({ password, password2, validation, handleOnChange, handleOnClickConfirm }) => {
  return (
    <div className='card card-login login'>
      <h3>Veuillez changer votre mot de passe avant de continuer.</h3>
      <div className='info-mdp'>Utiliser au moins 6 caractères.</div>
      <form className='form'>
        <div className='form-field'>
          <label>Nouveau mot de passe</label>
          <input type='password' name='password' value={password} onChange={handleOnChange} />
        </div>
        <div className='form-field'>
          <label>Confirmer le nouveau mot de passe</label>
          <input type='password' name='password2' value={password2} onChange={handleOnChange} />
          <div className='form-field--error'>{validation}</div>
        </div>
        <input
          type='submit'
          className='button'
          // style={{ width: '100%' }}
          onClick={handleOnClickConfirm}
          value='Modifier le mot de passe'
        />
      </form>
    </div>
  )
}

export default FormChange
