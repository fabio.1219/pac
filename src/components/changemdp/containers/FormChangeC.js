import FormChange from '../presentationals/FormChange'
import React, { Component } from 'react'
import { connect } from 'react-redux'
import { actions } from 'redux/login'
import Loader from 'components/global/loader/Loader'

class FormChangeC extends Component {
  state = {
    password: '',
    password2: '',
    validation: ''
  }

  handleOnChange = e => {
    const { name, value } = e.target
    this.setState({ [name]: value })
  }

  handleOnClickConfirm = async e => {
    e.preventDefault()
    const { password, password2 } = this.state
    if (password === password2) {
      if (password.length > 5) {
        this.setState({ validation: '' })
        this.props.postNewPassword({ password })
      } else {
        this.setState({ validation: 'Le mot de passe doit au moins contenir 6 caractères.' })
      }
    } else {
      this.setState({ validation: 'Les mots de passe ne correspondent pas.' })
    }
  }

  render () {
    const { password, password2, validation } = this.state
    if (this.props.isFetching) {
      return <div className='card card-login login'><Loader /></div>
    }

    return (
      <div>
        <FormChange
          password={password}
          password2={password2}
          validation={validation}
          handleOnChange={this.handleOnChange}
          handleOnClickConfirm={this.handleOnClickConfirm}
        />
      </div>
    )
  }
}

const mapStateToProps = state => {
  return {
    isFetching: state.login.isFetching
  }
}

const mapDispatchToProps = { postNewPassword: actions.postNewPassword }

FormChangeC = connect(mapStateToProps, mapDispatchToProps)(FormChangeC)

export default FormChangeC
