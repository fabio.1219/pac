import { get, post, put, del } from './api'
import URL_SERVER from './urlServer'

export function getClients (params = {}) {
  return get(`${URL_SERVER}/clients`, params)
}

export function getClient (id) {
  return get(`${URL_SERVER}/clients/${id}`)
}

export function postClient (client) {
  return post(`${URL_SERVER}/clients`, client)
}

export function putClient (id, client) {
  return put(`${URL_SERVER}/clients/${id}`, client)
}

export function delClient (id) {
  return del(`${URL_SERVER}/clients/${id}`)
}
