import axios from 'axios'

const setAuthorizationHeader = token => {
  axios.defaults.headers.common['Authorization'] = `Bearer ${token}`
}

export default setAuthorizationHeader