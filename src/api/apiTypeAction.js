import { get, post, put, del } from './api'
import URL_SERVER from './urlServer'

export function getTypesAction (params = {}) {
  return get(`${URL_SERVER}/typesaction`, params)
}

export function getTypeAction (id) {
  return get(`${URL_SERVER}/typesaction/${id}`)
}

export function postTypeAction (typesaction) {
  return post(`${URL_SERVER}/typesaction`, typesaction)
}

export function putTypeAction (id, typesaction) {
  return put(`${URL_SERVER}/typesaction/${id}`, typesaction)
}

export function delTypeAction (id) {
  return del(`${URL_SERVER}/typesaction/${id}`)
}
