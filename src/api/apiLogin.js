import { get, post } from './api'
import URL_SERVER from './urlServer'

export function postLogin (utilisateur) {
  return post(`${URL_SERVER}/login`, utilisateur)
}

export function getLogout () {
  return get(`${URL_SERVER}/logout`)
}

export function postNewPassword (password) {
  return post(`${URL_SERVER}/login/password`, password)
}
