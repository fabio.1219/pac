import { get } from './api'
import URL_SERVER from './urlServer'

export function getEtats (params = {}) {
  return get(`${URL_SERVER}/etats`, params)
}

/* export function getCategorie (id, params = {}) {
  return get(`${URL_SERVER}/categories/${id}`, params)
} */

/* export function postClient (client) {
  return post(`${URL_SERVER}/clients`, client)
}

export function putClient (id, client) {
  return put(`${URL_SERVER}/clients/${id}`, client)
} */
