import { get, post, put, del } from './api'
import URL_SERVER from './urlServer'

export function getUtilisateurs (params = {}) {
  return get(`${URL_SERVER}/utilisateurs`, params)
}

export function getUtilisateur (id) {
  return get(`${URL_SERVER}/utilisateurs/${id}`)
}

export function postUtilisateur (utilisateur) {
  return post(`${URL_SERVER}/utilisateurs`, utilisateur)
}

export function putUtilisateur (id, utilisateur) {
  return put(`${URL_SERVER}/utilisateurs/${id}`, utilisateur)
}

export function delUtilisateur (id) {
  return del(`${URL_SERVER}/utilisateurs/${id}`)
}
