import { get } from './api'
import URL_SERVER from './urlServer'

/* export function getClients (params = {}) {
  return get(`${URL_SERVER}/clients`, params)
} */

export function getCategorie (idCategorie, idReferant, params = {}) {
  return get(`${URL_SERVER}/categories/${idCategorie}/${idReferant}`, params)
}

/* export function postClient (client) {
  return post(`${URL_SERVER}/clients`, client)
}

export function putClient (id, client) {
  return put(`${URL_SERVER}/clients/${id}`, client)
} */
