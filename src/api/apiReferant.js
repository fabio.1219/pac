import { get } from './api'
import URL_SERVER from './urlServer'

export function getReferants () {
  return get(`${URL_SERVER}/referants`)
}
