import { post } from './api'
import URL_SERVER from './urlServer'

export function postCompterendu (compterendu) {
  return post(`${URL_SERVER}/comptesrendus`, compterendu)
}
