import { get, post, del } from './api'
import URL_SERVER from './urlServer'

export function getStatistiqueClients () {
  return get(`${URL_SERVER}/statistiques/clients`)
}

export function getStatistiquesSaved (params = {}) {
  return get(`${URL_SERVER}/statistiques/saved`, params)
}

export function postStatistiques (statistiques) {
  return post(`${URL_SERVER}/statistiques/saved`, statistiques)
}

export function delStatistiquesSaved (id) {
  return del(`${URL_SERVER}/statistiques/saved/${id}`)
}
