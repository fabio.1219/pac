import { get, post, put } from './api'
import URL_SERVER from './urlServer'

export function getActions (params = {}) {
  return get(`${URL_SERVER}/actions`, params)
}

export function getAction (id) {
  return get(`${URL_SERVER}/actions/${id}`)
}

export function postAction (action) {
  return post(`${URL_SERVER}/actions`, action)
}

export function putAction (id, action) {
  return put(`${URL_SERVER}/actions/${id}`, action)
}
