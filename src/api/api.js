import axios from 'axios'

export async function get (url, params = {}) {
  try {
    const response = await axios.get(url, { params })
    return response.data
  } catch (error) {
    console.error(error)
    return error.data
  }
}

export function post (url, content) {
  return axios.post(url, JSON.stringify(content)).then(response => response.data).catch(error => {
    const newWindow = window.open('')
    newWindow.document.write(error.response.data) // permet d'ouvrir dans une nouvelle fenetre les erreurs php
    return error.data
  })
}

export function put (url, content) {
  return axios.put(url, JSON.stringify(content)).then(response => response.data).catch(error => {
    const newWindow = window.open('')
    newWindow.document.write(error.response.data) // permet d'ouvrir dans une nouvelle fenetre les erreurs php
    return error.data
  })
}

export async function del (url) {
  try {
    const response = await axios.delete(url)
    return response.data
  } catch (error) {
    console.error(error)
    return error.data
  }
}
