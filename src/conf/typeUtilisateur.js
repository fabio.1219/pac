const RESPONSABLE_COMMERICAL = 0
const RESPONSABLE_SAISIE = 1
const REFERANT = 2
const NORMAL = 3
const NOM_TYPE = ['Responsable commercial', 'Responsable saisie', 'Référant', 'Utilisateur normal']

export { RESPONSABLE_COMMERICAL, RESPONSABLE_SAISIE, REFERANT, NORMAL, NOM_TYPE }
