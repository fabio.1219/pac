import { getReferants } from '../api/apiReferant'
import makeModule from './myReduxHelper'

const initialState = {
  isFetching: false,
  referants: []
}

const handlers = {}

const handlersAsync = {
  getReferants: {
    apiCall: getReferants,
    request (state) {
      return { ...state, isFetching: true }
    },
    success (state, { response }) {
      return { ...state, isFetching: false, referants: response }
    }
  }
}

const { reducer, actions } = makeModule('client', initialState, handlers, handlersAsync)

export default reducer
export { actions }
