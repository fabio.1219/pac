import { getAction, putAction } from '../api/apiAction'
import makeModule from './myReduxHelper'

const initialState = {
  isFetching: false,
  actionLoaded: false,
  showModal: false,
  etatModal: '',
  showModalFin: false,
  etatModalFin: '',
  client: {},
  typeaction: {},
  etatActuel: { etat: {} },
  etataction: []
}

const handlers = {
  openAction (state, { action }) {
    return { ...state, ...action, actionLoaded: true }
  },
  changeModal (state, { etatModal = '' }) {
    return { ...state, showModal: !state.showModal, etatModal }
  },
  changeModalFin (state, { etatModalFin = '' }) {
    return { ...state, showModalFin: !state.showModalFin, etatModalFin }
  }
}

const handlersAsync = {
  getAction: {
    apiCall: getAction,
    request (state) {
      return { ...state, isFetching: true }
    },
    success (state, { response }) {
      return { isFetching: false, ...response, actionLoaded: true }
    },
    error (state, error) {
      return { ...state, isFetching: false }
    }
  },
  updateAction: {
    apiCall: putAction,
    request (state) {
      return {
        ...state,
        showModal: false,
        showModalFin: false,
        etatModal: '',
        etatModalFin: '',
        isFetching: true
      }
    },
    success (state, { response }) {
      return { ...state, isFetching: false, ...response.data }
    },
    error (state, error) {
      return { ...state, isFetching: false }
    }
  }
}

const { reducer, actions } = makeModule('action', initialState, handlers, handlersAsync)

export default reducer
export { actions }
