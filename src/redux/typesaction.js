import getDateHeure from '../lib/DateFormat'
import { getTypesAction, putTypeAction, postTypeAction, delTypeAction } from '../api/apiTypeAction'
import makeModule from './myReduxHelper'

// reducer
const initialState = {
  isFetching: false,
  showForm: false,
  editMode: false,
  addMode: true,
  lastUpdated: 0,
  data: [],
  form: {},
  formOriginal: {}
}

const handlers = {
  open (state, { typeaction }) {
    return {
      ...state,
      form: typeaction,
      formOriginal: typeaction,
      editMode: false,
      showForm: true,
      addMode: false
    }
  },
  changeForm (state, { change }) {
    return { ...state, form: { ...state.form, ...change } }
  },
  switchEditMode (state) {
    return { ...state, editMode: !state.editMode }
  },
  cancelChangeForm (state) {
    return { ...state, form: state.formOriginal, editMode: false }
  },
  newForm (state) {
    return {
      ...state,
      showForm: true,
      editMode: true,
      form: { nom: '', categories: [] },
      formOriginal: { nom: '', categories: [] },
      addMode: true
    }
  },
  closeForm (state) {
    return { ...state, showForm: false, editMode: false }
  }
}

const handlersAsync = {
  getTypesAction: {
    apiCall: getTypesAction,
    request (state) {
      return { ...state, isFetching: true }
    },
    success (state, { response }) {
      return { ...state, isFetching: false, lastUpdated: getDateHeure(), ...response }
    },
    error (state) {
      return state
    }
  },
  updateTypeAction: {
    apiCall: putTypeAction,
    request (state) {
      return { ...state, isFetching: true }
    },
    success (state, { response }) {
      const typeActionUpdated = response.data
      const data = state.data.map(
        typeaction => (typeaction.id !== typeActionUpdated.id ? typeaction : typeActionUpdated)
      )
      return {
        ...state,
        isFetching: false,
        data,
        lastUpdated: getDateHeure(),
        form: typeActionUpdated,
        formOriginal: typeActionUpdated,
        editMode: false
      }
    },
    error (state) {
      return state
    }
  },
  addTypeAction: {
    apiCall: postTypeAction,
    request (state) {
      return { ...state, isFetching: true }
    },
    success (state, { response }) {
      const typeActionAdded = {
        ...state.form,
        id: response.id
      }
      return {
        ...state,
        isFetching: false,
        form: typeActionAdded,
        formOriginal: typeActionAdded,
        editMode: false,
        addMode: false
      }
    },
    error (state) {
      return state
    }
  },
  delTypeAction: {
    apiCall: delTypeAction,
    request (state) {
      return { ...state, isFetching: true }
    },
    success (state, { response }) {
      return { ...state, isFetching: false, showForm: false }
    }
  }
}

const { reducer, actions } = makeModule('typesAction', initialState, handlers, handlersAsync)
export default reducer
export { actions }
