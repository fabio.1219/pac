import { getClient, putClient, delClient } from '../api/apiClient'
import { getReferants } from '../api/apiReferant'
import { postCompterendu } from '../api/apiCompterendu'
import makeModule from './myReduxHelper'

const initialState = {
  etatTab: 'PartieClient',
  isFetching: false,
  client: {},
  clientEdited: {},
  validation: {},
  referants: [],
  editMode: false
}

const handlers = {
  setEtatTab (state, { etatTab }) {
    return { ...state, etatTab }
  },
  editModeClient (state) {
    return { ...state, editMode: true }
  },
  cancelEditClient (state) {
    return { ...state, editMode: false, clientEdited: {...state.client}, validation: {} }
  },
  updateFormClient (state, { change }) {
    return { ...state, clientEdited: { ...state.clientEdited, ...change } }
  },
  validateClient (state, { validation }) {
    return { ...state, validation, etatTab: 'PartieClient' }
  },
  deleteContact (state, { index }) {
    let contacts = state.clientEdited.contacts.slice()
    contacts[index] = { ...contacts[index], deleted: true }
    return {
      ...state,
      clientEdited: {
        ...state.clientEdited,
        contacts
      }
    }
  },
  addContact (state, { contact }) {
    let contacts = state.clientEdited.contacts.slice()
    contacts.push(contact)
    return {
      ...state,
      clientEdited: {
        ...state.clientEdited,
        contacts
      }
    }
  },
  updateContact (state, { contact, index }) {
    let contacts = state.clientEdited.contacts.slice()
    contacts[index] = contact
    return {
      ...state,
      clientEdited: {
        ...state.clientEdited,
        contacts
      }
    }
  }
}

const handlersAsync = {
  getClient: {
    apiCall: getClient,
    request (state) {
      return { ...state, isFetching: true, validation: {}, editMode: false }
    },
    success (state, { response }) {
      return { ...state, client: response, clientEdited: response, isFetching: false }
    }
  },
  updateClient: {
    apiCall: putClient,
    request (state) {
      return { ...state, isFetching: true, editMode: false, validation: {} }
    },
    success (state, { response }) {
      return {
        ...state,
        isFetching: false,
        client: response.data,
        clientEdited: response.data,
        etatTab: 'PartieClient'
      }
    }
  },
  delClient: {
    apiCall: delClient,
    request (state) {
      return { ...state, isFetching: true }
    },
    success (state) {
      return initialState
    }
  },
  getReferants: {
    apiCall: getReferants,
    request (state) {
      return { ...state, isFetching: true }
    },
    success (state, { response }) {
      return { ...state, isFetching: false, referants: response }
    }
  },
  postCompterendu: {
    apiCall: postCompterendu,
    request (state) {
      return { ...state, isFetching: true }
    },
    success (state) {
      return { ...state, isFetching: false }
    }
  }
}

const { reducer, actions } = makeModule('client', initialState, handlers, handlersAsync)

export default reducer
export { actions }
