import getDateHeure from '../lib/DateFormat'
import { getClients } from '../api/apiClient'
import makeModule from './myReduxHelper'

const initialState = {
  isFetching: false,
  lastUpdated: 0,
  data: []
}

const handlersAsync = {
  getClients: {
    apiCall: getClients,
    request (state) {
      return { ...state, isFetching: true }
    },
    success (state, { response }) {
      return { ...state, isFetching: false, lastUpdated: getDateHeure(), ...response }
    }
  }
}

const { reducer, actions } = makeModule('clients', initialState, undefined, handlersAsync)
export default reducer
export { actions }
