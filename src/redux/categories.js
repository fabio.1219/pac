import getDateHeure from '../lib/DateFormat'
import { getCategorie } from '../api/apiCategorie'
import makeModule from './myReduxHelper'

const initialState = {
  isFetching: false,
  lastUpdated: 0,
  categorie: -1,
  referant: { id: -1 },
  data: []
}

const handlers = {
  choixCategorie (state, { categorie }) {
    return { ...state, categorie }
  },
  choixReferant (state, { referant }) {
    return { ...state, referant }
  },
  reinit (state) {
    return initialState
  }
}

const handlersAsync = {
  getClientsCategorie: {
    apiCall: getCategorie,
    request (state) {
      return { ...state, isFetching: true }
    },
    success (state, { response }) {
      return { ...state, isFetching: false, lastUpdated: getDateHeure(), ...response }
    }
  }
}

const { reducer, actions } = makeModule('clientsCategorie', initialState, handlers, handlersAsync)
export default reducer
export { actions }
