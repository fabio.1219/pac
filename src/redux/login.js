import { getLogout, postLogin, postNewPassword } from '../api/apiLogin'
import makeModule from './myReduxHelper'
import setAuthorizationHeader from 'api/apiParam'
import { getSavedToken, clear, saveInCookie, saveInSessionStorage } from 'lib/CookieStorageHelper'

const initialState = {
  isFetching: false,
  isLogged: false,
  toCookie: false,
  error: '',
  token: '',
  decoded: {}
}

const handlers = {
  check (state) {
    const token = getSavedToken()
    if (token) {
      setAuthorizationHeader(token)
      const splited = token.split('.')
      const decoded = JSON.parse(atob(splited[1]))
      return { isFetching: false, isLogged: true, token, decoded }
    }
    return state
  },
  saveInCookie (state) {
    return { ...state, toCookie: true }
  }
}

const handlersAsync = {
  login: {
    apiCall: postLogin,
    request (state) {
      return { ...state, isFetching: true }
    },
    success (state, { response }) {
      if (response.status === 'success') {
        const splited = response.token.split('.')
        const decoded = JSON.parse(atob(splited[1]))
        setAuthorizationHeader(response.token)
        saveInSessionStorage(response.token)
        if (state.toCookie) {
          saveInCookie(response.token)
        }
        return {
          isFetching: false,
          isLogged: true,
          token: response.token,
          decoded
        }
      }
      return { ...state, error: "Nom d'utilisateur ou mot de passe incorrect. Veuillez réessayer." }
    },
    error (state, error) {
      return { ...state, isFetching: false }
    }
  },
  postNewPassword: {
    apiCall: postNewPassword,
    request (state) {
      return { ...state, isFetching: true }
    },
    success (state, { response }) {
      const splited = response.token.split('.')
      const decoded = JSON.parse(atob(splited[1]))
      setAuthorizationHeader(response.token)
      saveInSessionStorage(response.token)
      if (state.toCookie) {
        saveInCookie(response.token)
      }
      return {
        isFetching: false,
        isLogged: true,
        token: response.token,
        decoded
      }
    },
    error (state) {
      return { ...state, isFetching: false }
    }
  },
  logout: {
    apiCall: getLogout,
    request (state) {
      return { ...state, isFetching: true }
    },
    success (state, { response }) {
      clear()
      return {
        ...initialState
      }
    }
  }
}

const { reducer, actions } = makeModule('login', initialState, handlers, handlersAsync)

export default reducer
export { actions }
