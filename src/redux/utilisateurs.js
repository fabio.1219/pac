import getDateHeure from '../lib/DateFormat'
import {
  getUtilisateurs,
  getUtilisateur,
  postUtilisateur,
  putUtilisateur,
  delUtilisateur
} from '../api/apiUtilisateur'
import makeModule from './myReduxHelper'

const initialState = {
  isFetching: false,
  lastUpdated: 0,
  data: [],
  utilisateur: {
    nom: '',
    login: '',
    type: ''
  },
  utilisateurEdited: {
    nom: '',
    login: '',
    type: '',
    validationId: ''
  },
  utilisateurNew: {
    nom: '',
    login: '',
    type: '',
    validationId: ''
  }
}

const handlers = {
  updateFormUtilisateurEdited (state, { change }) {
    return { ...state, utilisateurEdited: { ...state.utilisateurEdited, ...change } }
  },
  updateFormUtilisateurNew (state, { change }) {
    return { ...state, utilisateurNew: { ...state.utilisateurNew, ...change } }
  },
  updateValidationId (state, { validationId }) {
    return { ...state, validationId }
  }
}

const handlersAsync = {
  getUtilisateurs: {
    apiCall: getUtilisateurs,
    request (state) {
      return { ...state, isFetching: true }
    },
    success (state, { response }) {
      return { ...state, isFetching: false, lastUpdated: getDateHeure(), ...response }
    }
  },
  getUtilisateur: {
    apiCall: getUtilisateur,
    request (state) {
      return { ...state, isFetching: true }
    },
    success (state, { response }) {
      return { ...state, isFetching: false, utilisateur: response, utilisateurEdited: response }
    }
  },
  addUtilisateur: {
    apiCall: postUtilisateur,
    request (state) {
      return { ...state, isFetching: true }
    },
    success (state, { response }) {
      if (response.status === 'loginExist') {
        return {
          ...state,
          isFetching: false,
          utilisateurNew: {
            ...state.utilisateurNew,
            validationId: 'Cet identifiant existe déjà. Veuillez en choisir un autre.'
          }
        }
      }
      return {
        ...state,
        isFetching: false,
        utilisateurNew: {
          ...state.utilisateurNew,
          validationId: ''
        }
      }
    }
  },
  updateUtilisateur: {
    apiCall: putUtilisateur,
    request (state) {
      return { ...state, isFetching: true }
    },
    success (state, { response }) {
      if (response.status === 'loginExist') {
        return {
        ...state,
        isFetching: false,
        utilisateurEdited: {...state.utilisateurEdited, validationId: 'Cet identifiant existe déjà. Veuillez en choisir un autre.'}
        }
      }
      return {
        ...state,
        isFetching: false,
        utilisateur: response.data,
        utilisateurEdited: {...response.data, validationId: '' }
      }
    }
  },
  delUtilisateur: {
    apiCall: delUtilisateur,
    request (state) {
      return { ...state, isFetching: true }
    },
    success (state, { response }) {
      return initialState
    }
  }
}

const { reducer, actions } = makeModule('utilisateurs', initialState, handlers, handlersAsync)
export default reducer
export { actions }
