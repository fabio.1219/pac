import {
  getStatistiqueClients,
  getStatistiquesSaved,
  postStatistiques,
  delStatistiquesSaved
} from '../api/apiStatistique'
import makeModule from './myReduxHelper'

const initialState = {
  isFetchingCurrent: true,
  isFetchingSaved: true,
  actuel: {},
  isSelected: false,
  select: {},
  statsSaved: {},
  arrayColor: []
}

const handlers = {
  open (state, { statistiques, isSelected }) {
    return { ...state, select: statistiques, isSelected }
  },
  updateArrayColor (state, { arrayColor }) {
    return { ...state, arrayColor }
  }
}

const handlersAsync = {
  getStatistiqueClients: {
    apiCall: getStatistiqueClients,
    request (state) {
      return { ...state, isFetchingCurrent: true }
    },
    success (state, { response }) {
      return {
        ...state,
        isFetchingCurrent: false,
        actuel: { id: 0, ...response },
        select: { id: 0, ...response }
      }
    }
  },
  getStatistiquesSaved: {
    apiCall: getStatistiquesSaved,
    request (state) {
      return { ...state, isFetchingSaved: true }
    },
    success (state, { response }) {
      return { ...state, isFetchingSaved: false, statsSaved: response }
    }
  },
  postStatistiques: {
    apiCall: postStatistiques,
    request (state) {
      return { ...state, isFetchingSaved: true }
    },
    success (state, { response }) {
      return { ...state, isFetchingSaved: false }
    }
  },
  delStatistiquesSaved: {
    apiCall: delStatistiquesSaved,
    request (state) {
      return { ...state, isFetchingSaved: true }
    },
    success (state) {
      return { ...state, select: { ...state.actuel }, isFetchingSaved: false }
    }
  }
}

const { reducer, actions } = makeModule('statistiques', initialState, handlers, handlersAsync)

export default reducer
export { actions }
