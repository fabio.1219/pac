import { combineReducers } from 'redux'
import clients from './clients'
import client from './client'
import utilisateurs from './utilisateurs'
import login from './login'
import categories from './categories'
import typesaction from './typesaction'
import entreprendreaction from './entreprendreaction'
import actions from './actions'
import action from './action'
import etats from './etats'
import referants from './referants'
import statistiques from './statistiques'

const pacApp = combineReducers({
  login,
  clients,
  client,
  utilisateurs,
  categories,
  typesaction,
  entreprendreaction,
  action,
  actions,
  referants,
  etats,
  statistiques
})

export default pacApp
