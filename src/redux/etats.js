import { getEtats } from '../api/apiEtat'
import makeModule from './myReduxHelper'

const initialState = {
  isFetching: false,
  lastUpdated: 0,
  etats: []
}

const handlers = undefined

const handlersAsync = {
  getEtats: {
    apiCall: getEtats,
    request (state) {
      return { ...state, isFetching: true }
    },
    success (state, { response }) {
      return { ...state, isFetching: false, etats: response }
    },
    error (state, error) {
      return { ...state, isFetching: false }
    }
  }
}

const { reducer, actions } = makeModule('etats', initialState, handlers, handlersAsync)

export default reducer
export { actions }
