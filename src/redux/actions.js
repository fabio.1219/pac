import { getActions } from '../api/apiAction'
import getDateHeure from '../lib/DateFormat'
import makeModule from './myReduxHelper'

const initialState = {
  isFetching: false,
  lastUpdated: 0,
  data: []
}

const handlers = {
  openClient (state, { client }) {
    return { ...state, client }
  }
}

const handlersAsync = {
  getActions: {
    apiCall: getActions,
    request (state) {
      return { ...state, isFetching: true }
    },
    success (state, { response }) {
      return { ...state, isFetching: false, lastUpdated: getDateHeure(), ...response }
    },
    error (state, error) {
      return { ...state, isFetching: false }
    }
  }
}

const { reducer, actions } = makeModule('actions', initialState, handlers, handlersAsync)

export default reducer
export { actions }
