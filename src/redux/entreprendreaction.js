import { getClient } from '../api/apiClient'
import { getTypesAction } from '../api/apiTypeAction'
import { postAction } from '../api/apiAction'
import makeModule from './myReduxHelper'

const initialState = {
  isFetching: false,
  client: {},
  typesaction: []
}

const handlers = {
  openClient (state, { client }) {
    return { ...state, client }
  }
}

const handlersAsync = {
  getClient: {
    apiCall: getClient,
    request (state) {
      return { ...state, isFetching: true }
    },
    success (state, { response }) {
      return { isFetching: false, client: response }
    },
    error (state, error) {
      return { ...state, isFetching: false }
    }
  },
  getTypesaction: {
    apiCall: getTypesAction,
    request (state) {
      return { ...state, isFetching: true }
    },
    success (state, { response }) {
      return { ...state, isFetching: false, typesaction: response }
    },
    error (state, error) {
      return { ...state, isFetching: false }
    }
  },
  createAction: {
    apiCall: postAction,
    request (state) {
      return { ...state, isFetching: true }
    },
    success (state, { response }) {
      return { ...state, isFetching: false }
    },
    error (state, error) {
      return { ...state, isFetching: false }
    }
  }
}

const { reducer, actions } = makeModule('entreprendreAction', initialState, handlers, handlersAsync)

export default reducer
export { actions }
